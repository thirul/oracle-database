// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.util.BaseConnection;

/**
 * The Class DatabaseConnectorConnection.
 *
 * @author swastik.vn
 */
public class OracleDatabaseConnection extends BaseConnection {

	/** The class name. */
	private String className;

	/** The username. */
	private String username;

	/** The password. */
	private String password;

	/** The url. */
	private String url;

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(OracleDatabaseConnection.class.getName());

	/** The custom property. */
	private Map<String, String> customProperty;

	/**
	 * Instantiates a new database connector connection.
	 *
	 * @param context the context
	 */
	public OracleDatabaseConnection(BrowseContext context) {
		super(context);
		this.className = getContext().getConnectionProperties().getProperty(CLASSNAME,"");
		this.username = getContext().getConnectionProperties().getProperty(USERNAME,"");
		this.password = getContext().getConnectionProperties().getProperty(PASS,"");
		this.url = getContext().getConnectionProperties().getProperty(URL,"");
		this.customProperty = context.getConnectionProperties().getCustomProperties("connectionProperties");
	}

	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Load properties.
	 *
	 * @return the properties
	 */
	public Properties loadProperties() {
		Properties prop = new Properties();
		if (!getUsername().isEmpty()) {
			prop.put("user", getUsername());
		}
		if (!getPassword().isEmpty()) {
			prop.put("password", getPassword());
		}
		if (customProperty != null && !customProperty.isEmpty()) {
			for (Map.Entry<String, String> entry : customProperty.entrySet()) {
				prop.put(entry.getKey(), entry.getValue());
			}
		}
		return prop;

	}

	/**
	 * Gets the solo connection.
	 *
	 * @return the solo connection
	 */
	public Driver getSoloConnection() {

		try {
			// In order to allow the ability to invoke multiple database drivers at the same
			// time, the DriverManager
			// class needs to be initialized before Class.forName is called. Calling this
			// particular method will invoke
			// the DriverManager so it is class loaded before trying to instantiate drivers.
			DriverManager.getLoginTimeout();
			return (Driver) Class.forName(className).newInstance();
		} catch (ClassNotFoundException e) {
			throw new ConnectorException("Failed Loading the class");
		} catch (InstantiationException | IllegalAccessException e) {
			throw new ConnectorException(e.getMessage());
		}
	}

	/**
	 * Method to test the Database Connection by taking the Standard JDBC
	 * Parameters.
	 *
	 * @throws ConnectorException the connector exception
	 */
	public void test() {
		try (Connection conn = getSoloConnection().connect(url, loadProperties());) {
			logger.log(Level.FINE, "Connection established successfully");
		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}

	}

}