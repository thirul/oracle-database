// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.upsert;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;

import java.io.IOException;
import java.io.InputStream;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.DynamicInsertOperation;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;
import com.boomi.connector.oracledatabase.model.BatchResponse;
import com.boomi.connector.oracledatabase.model.QueryResponse;
import com.boomi.connector.oracledatabase.util.CustomResponseUtil;
import com.boomi.connector.oracledatabase.util.MetadataUtil;
import com.boomi.connector.oracledatabase.util.OracleDatabaseConstants;
import com.boomi.connector.oracledatabase.util.QueryBuilderUtil;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.util.IOUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * The Class DynamicUpsert.
 *
 * @author swastik.vn
 */
public class DynamicUpsert extends SizeLimitedUpdateOperation{


	/**
	 * Instantiates a new Dynamic upsert.
	 *
	 * @param con          the OracleDatabaseConnection
	 */
	public DynamicUpsert(OracleDatabaseConnection conn) {
	super(conn);
	}

	/** The mapper. */
	ObjectMapper mapper = new ObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)
			.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(DynamicUpsert.class.getName());
	
	@Override
	public void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		OracleDatabaseConnection conn = getConnection();
		PropertyMap map = getContext().getOperationProperties();
		Long batchCount = map.getLongProperty(OracleDatabaseConstants.BATCH_COUNT);
		String commitOption = map
				.getProperty(OracleDatabaseConstants.COMMIT_OPTION);
		try (Connection con = conn.getSoloConnection().connect(conn.getUrl(), conn.loadProperties())) {
			con.setAutoCommit(false);
			String primaryKey = MetadataUtil.getPrimaryKey(con, getContext().getObjectTypeId());
			if(!primaryKey.isEmpty()) {
				this.executeStatements(request, response, con, batchCount, commitOption, primaryKey);	
			}else {
				DynamicInsertOperation ins = new DynamicInsertOperation(getConnection());
				ins.executeStatements(con, request, response, batchCount, commitOption);
			}
		} catch (Exception e) {
			ResponseUtil.addExceptionFailures(response, request, e);
		}

	}
	/**
	 * This method is the entry point for the Upsert Logic. This method will take
	 * the UpdateRequest and builds the SQL Statements and Executes them based on
	 * the Commit Options.
	 *
	 * @param trackedData the tracked data
	 * @param response    the response
	 * @param commitOption 
	 * @param batchCount 
	 * @param commitOption 
	 * @param batchCount 
	 * @param primaryKey 
	 * @throws SQLException            the SQL exception
	 * @throws JsonProcessingException the json processing exception
	 */
	public void executeStatements(UpdateRequest trackedData, OperationResponse response, Connection con, Long batchCount, String commitOption, String primaryKey)
			throws SQLException, JsonProcessingException {
		// This Map will be getting the data type of the each column associated with the
		// table.
		Map<String, String> dataTypes = new MetadataUtil(con, getContext().getObjectTypeId()).getDataType();

		// We are extending SizeLimitUpdate Operation it loads only single document into
		// memory. Hence we are preparing the list of Object Data which will be required
		// for Statement batching and for creating the Query for Prepared Statement.
		List<ObjectData> batchData = new ArrayList<>();
		for (ObjectData objdata : trackedData) {
			batchData.add(objdata);
		}
		StringBuilder query = this.buildPreparedStatement(batchData, dataTypes, con, primaryKey);
		if (batchCount != null && batchCount > 0 && commitOption.equals(COMMIT_BY_ROWS)) {
			this.doBatch(dataTypes, response, batchData, query, con, batchCount, primaryKey);
		} else if (batchCount == null || batchCount == 0 || commitOption.equals(COMMIT_BY_PROFILE)) {
				Payload payload = null;
				try (PreparedStatement pstmnt = con.prepareStatement(query.toString())) {
					for (ObjectData objdata : batchData) {
						try {
							
							this.appendParams(objdata, dataTypes, pstmnt, con, primaryKey);
							
							int effectedRowCount = pstmnt.executeUpdate();
							
							payload = JsonPayloadUtil
									.toPayload(new QueryResponse(query.toString(), effectedRowCount, "Executed Successfully"));
							ResponseUtil.addSuccess(response, objdata, SUCCESS_RESPONSE_CODE, payload);
							con.commit();
						 } catch (SQLException e) {
							CustomResponseUtil.writeSqlErrorResponse(e, objdata, response);
						} catch (IOException e) {
							CustomResponseUtil.writeErrorResponse(e, objdata, response);
						} catch (ConnectorException e) {
							ResponseUtil.addExceptionFailure(response, objdata, e);
						}
				
					}
				} finally {
					IOUtil.closeQuietly(payload);
					try {
						con.commit();
					} catch (SQLException e) {
						logger.log(Level.SEVERE, e.getMessage());
					}
				}
		} else if (batchCount < 0) {
			throw new ConnectorException("Batch count cannot be negative");
		}

	}

	/**
	 * This method will build the SQL Statements based on the conflict. If conflict
	 * is present it will build Insert statement orelse Update
	 *
	 * @param objdata   the objdata
	 * @param dataTypes the data types
	 * @param primaryKey 
	 * @param isBatch   the is batch
	 * @return the string builder
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	private StringBuilder buildStatements(ObjectData objdata, Map<String, String> dataTypes, Connection con, String primaryKey)
			throws SQLException, IOException {
		StringBuilder query = new StringBuilder();
		// This List will be holding the names of the columns which will satisfy the
		// Primary Key and Unique Key constraints if any.
		String tableName = getContext().getObjectTypeId();
		if (!primaryKey.isEmpty()) {
			query = QueryBuilderUtil.buildUpsertQuery(tableName, primaryKey, dataTypes);
		} else  {
			query = QueryBuilderUtil.buildInitialQuery(con, tableName);
			this.buildInsertQuery(query, objdata, con);
		} 
		return query;
	}

	/**
	 * Does the JDBC Statement batching if the batch count is greater than zero and
	 * commit option is commit by rows. This method will take the input request and
	 * Builds the SQL Statements and does the batching.
	 *
	 * @param dataTypes the data types
	 * @param response  the response
	 * @param batchData the batch data
	 * @param query the query
	 * @param batchCount 
	 * @param con 
	 * @param primaryKey 
	 * @throws SQLException the SQL exception
	 */
	private void doBatch(Map<String, String> dataTypes, OperationResponse response, List<ObjectData> batchData,
			StringBuilder query, Connection con, Long batchCount, String primaryKey) throws SQLException {
		int batchnum = 0;
		int b = 0;
		boolean shouldExecute = true;
		// Note: Here the PreparedStatement will be held in the memory. This issue has
		// been addressed in dbv2 connector. We will be informing the user to use batch
		// count less than 10 to limit the memory been held for some extent.
		try (PreparedStatement bstmnt = con.prepareStatement(query.toString())) {
			for (ObjectData objdata : batchData) {
				b++;
				Payload payload = null;
				try {
					this.appendParams(objdata, dataTypes, bstmnt, con, primaryKey);
					bstmnt.addBatch();
					if (b == batchCount) {
						batchnum++;
						if (shouldExecute) {
							int res[] = bstmnt.executeBatch();
							bstmnt.clearParameters();
							con.commit();
							response.getLogger().log(Level.INFO, BATCH_NUM + batchnum);
							response.getLogger().log(Level.INFO, BATCH_RECORDS + res.length);
							payload = JsonPayloadUtil
									.toPayload(new BatchResponse("Batch executed successfully", batchnum, res.length));
							response.addResult(objdata, OperationStatus.SUCCESS, SUCCESS_RESPONSE_CODE,
									SUCCESS_RESPONSE_MESSAGE, payload);
						} else {
							bstmnt.clearBatch();
							shouldExecute = true;
							CustomResponseUtil.logFailedBatch(response, batchnum, b);
							CustomResponseUtil.batchExecuteError(objdata, response, batchnum, b);
						}
						b = 0;
					} else if (b < batchCount) {
						int remainingBatch = batchnum + 1;
						if (batchData.lastIndexOf(objdata) == batchData.size() - 1) {
							this.executeRemaining(objdata, bstmnt, response, remainingBatch, con, b);
						} else {
							payload = JsonPayloadUtil.toPayload(
									new BatchResponse("Record added to batch successfully", remainingBatch, b));
							ResponseUtil.addSuccess(response, objdata, SUCCESS_RESPONSE_CODE, payload);
						}

					}

				} catch (BatchUpdateException e) {
					CustomResponseUtil.logFailedBatch(response, batchnum, b);
					CustomResponseUtil.batchExecuteError(objdata, response, batchnum, b);
					b = 0;
				} catch (SQLException e) {
					CustomResponseUtil.logFailedBatch(response, batchnum, b);
					shouldExecute = this.checkLastRecord(b, batchCount);
					if (shouldExecute) {
						b = 0;
					}
					CustomResponseUtil.writeSqlErrorResponse(e, objdata, response);
				} catch (IOException | IllegalArgumentException e) {
					shouldExecute = this.checkLastRecord(b, batchCount);
					if (shouldExecute || batchData.lastIndexOf(objdata) == batchData.size() - 1) {
						bstmnt.clearBatch();
						batchnum++;
						CustomResponseUtil.logFailedBatch(response, batchnum, b);
						b = 0;
					}
					CustomResponseUtil.writeErrorResponse(e, objdata, response);
				} catch (ConnectorException e) {
					ResponseUtil.addExceptionFailure(response, objdata, e);
				}

				finally {
					IOUtil.closeQuietly(payload);
				}
			}

		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}

	}

	/**
	 * This method will check if any violation exists in the table and decides
	 * whether to form insert statement or update.
	 *
	 * @param objdata   the objdata
	 * @param dataTypes the data types
	 * @param bstmnt    the bstmnt
	 * @param primaryKey 
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	private void appendParams(ObjectData objdata, Map<String, String> dataTypes, PreparedStatement bstmnt, Connection con, String primaryKey)
			throws SQLException, IOException {
		if (!primaryKey.isEmpty()) {
			this.appendUpsertParams(bstmnt, objdata, dataTypes, primaryKey);
		} else {
			this.appendInsertParams(bstmnt, objdata, dataTypes, con);
		}
	}

	/**
	 * This method will append the request values to the placeholder in the Prepared
	 * Statements. The agenda behind this method is to append values to below
	 * mentioned Query where we have to iterate over input request twice MERGE INTO
	 * CUSTOMER USING DUAL ON (CUSTOMER_ID = ?) WHEN MATCHED THEN UPDATE SET
	 * CUSTOMER_NAME=?,CUSTOMER_CITY=?,CUSTOMER_DOB=?,CUSTOMER_COUTRY=?WHEN NOT
	 * MATCHED THEN INSERT
	 * (CUSTOMER_NAME,CUSTOMER_ID,CUSTOMER_CITY,CUSTOMER_DOB,CUSTOMER_COUTRY) VALUES
	 * (?,?,?,?,?)
	 * 
	 * @param bstmnt the bstmnt
	 * @param objdata the objdata
	 * @param dataTypes the data types
	 * @param pk the pk
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	private void appendUpsertParams(PreparedStatement bstmnt, ObjectData objdata, Map<String, String> dataTypes,
			String pk) throws IOException, SQLException {

		int i = 0;
		JsonNode json = null;
		try (InputStream is = objdata.getData()) {
			json = mapper.readTree(is);
			if (json != null) {
				i++;
				QueryBuilderUtil.checkDataType(bstmnt, dataTypes, pk, json.get(pk).toString().replace(BACKSLASH, ""), i);
				for (Map.Entry<String, String> entry : dataTypes.entrySet()) {
					if (!entry.getKey().equals(pk)) {
						i++;
						QueryBuilderUtil.checkDataType(bstmnt, dataTypes, entry.getKey(), json.get(entry.getKey()).toString().replace(BACKSLASH, ""), i);
					}
				}
				for (Map.Entry<String, String> entry : dataTypes.entrySet()) {
					i++;
					QueryBuilderUtil.checkDataType(bstmnt, dataTypes, entry.getKey(), json.get(entry.getKey()).toString().replace(BACKSLASH, ""), i);
				}
			}
			else {
				throw new ConnectorException(INPUT_ERROR);
			}

		}

	}

	/**
	 * This method will append the values to the Insert Query formed for the
	 * prepared Statements.
	 *
	 * @param bstmnt    the bstmnt
	 * @param objdata   the objdata
	 * @param dataTypes the data types
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	private void appendInsertParams(PreparedStatement bstmnt, ObjectData objdata, Map<String, String> dataTypes, Connection con)
			throws SQLException, IOException {
		int i = 0;
		JsonNode json = null;
		try (InputStream is = objdata.getData()) {
			json = mapper.readTree(is);
			if (json != null) {
				DatabaseMetaData md = con.getMetaData();
				try (ResultSet resultSet = md.getColumns(null, null, getContext().getObjectTypeId(), null)) {
					while (resultSet.next()) {
						String key = resultSet.getString(COLUMN_NAME);
						JsonNode fieldName = json.get(key);
						i++;
						QueryBuilderUtil.checkDataType(bstmnt, dataTypes, key.toUpperCase(), fieldName.toString().replace(BACKSLASH, ""), i);

					}
				}
			} else {
				throw new ConnectorException(INPUT_ERROR);
			}

		}

	}

	/**
	 * Builds the prepared statement by taking the 1st request of the tracked data.
	 * if 1st request is not proper or if it throws any exception it will move to
	 * subsequent requests until the query is formed.
	 *
	 * @param batchData the batch data
	 * @param dataTypes the data types
	 * @param primaryKey 
	 * @param b         the b
	 * @return the string builder
	 * @throws SQLException the SQL exception
	 */
	private StringBuilder buildPreparedStatement(List<ObjectData> batchData, Map<String, String> dataTypes, Connection con, String primaryKey)
			throws SQLException {
		StringBuilder query = new StringBuilder();
		for (ObjectData objdata : batchData) {
			try {
				query = this.buildStatements(objdata, dataTypes, con, primaryKey);
			} catch (IOException e) {
				// moving to next request
				logger.log(Level.SEVERE, e.toString());
			}
			if (query.length() != 0) {
				break;
			}
		}
		return query;
	}

	/**
	 * This method will build the Insert query based on the Input Request.
	 *
	 * @param query     the query
	 * @param objdata   the objdata
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	private void buildInsertQuery(StringBuilder query, ObjectData objdata, Connection con) throws SQLException, IOException {
		JsonNode json = null;
		try (InputStream is = objdata.getData()) {
			json = mapper.readTree(is);
			if (json != null) {
				DatabaseMetaData md = con.getMetaData();
				try (ResultSet resultSet = md.getColumns(null, null, getContext().getObjectTypeId(), null)) {
					while (resultSet.next()) {
						String key = resultSet.getString(COLUMN_NAME);
						JsonNode fieldName = json.get(key);
						if (fieldName != null) {
							query.append(PARAM);
						}

					}
				}

			} else {
				throw new ConnectorException(INPUT_ERROR);
			}
			query.deleteCharAt(query.length() - 1);
			query.append(")");
		}

	}

	/**
	 * This method will return the primary keys and the Unique Keys in the table.
	 * @param con 
	 *
	 * @return the primary keys
	 * @throws SQLException the SQL exception
	 */
	private String getPrimaryKey(Connection con) throws SQLException {
		List<String> pk = new ArrayList<>();
		try (ResultSet resultSet = con.getMetaData().getIndexInfo(null, null, getContext().getObjectTypeId(), true, false)) {
			while (resultSet.next()) {
				if (null != resultSet.getString(NON_UNIQUE)
						&& (resultSet.getString(NON_UNIQUE).equals("0") || resultSet.getString(NON_UNIQUE).equals("f"))
						&& resultSet.getString(COLUMN_NAME) != null && !pk.contains(resultSet.getString(COLUMN_NAME)))
					pk.add(resultSet.getString(COLUMN_NAME));
			}
		}
		if(pk.size()>1) {
			throw new ConnectorException(getContext().getObjectTypeId() + " has more than one primary keys!!");
		}else {
			return pk.get(0).isEmpty() ? "" : pk.get(0);
		}
	}

	/**
	 * This method will execute the remaining statements of the batch.
	 *
	 * @param data           the data
	 * @param execStatement  the exec statement
	 * @param response       the response
	 * @param remainingBatch the remaining batch
	 * @param con            the con
	 * @param b              the b
	 */
	private void executeRemaining(ObjectData data, PreparedStatement execStatement, OperationResponse response,
			int remainingBatch, Connection con, int b) {
		Payload payload = null;
		try {
			int[] res = execStatement.executeBatch();
			response.getLogger().log(Level.INFO, BATCH_NUM + remainingBatch);
			response.getLogger().log(Level.INFO, REMAINING_BATCH_RECORDS + res.length);
			payload = JsonPayloadUtil.toPayload(new BatchResponse(
					"Remaining records added to batch and executed successfully", remainingBatch, res.length));
			response.addResult(data, OperationStatus.SUCCESS, SUCCESS_RESPONSE_CODE, SUCCESS_RESPONSE_MESSAGE, payload);
			con.commit();
		} catch (SQLException e) {
			CustomResponseUtil.logFailedBatch(response, remainingBatch, b);
			CustomResponseUtil.writeSqlErrorResponse(e, data, response);
		} finally {
			IOUtil.closeQuietly(payload);
		}
	}

	/**
	 * This method will check whether the input is the last object data of the batch
	 * or not.
	 *
	 * @param b          the b
	 * @param batchCount the batch count
	 * @return if yes returns true or else return false
	 */

	private boolean checkLastRecord(int b, Long batchCount) {
		return b == batchCount;
	}
	
	/**
	 * Gets the Connection instance.
	 *
	 * @return the connection
	 */
	@Override
	public OracleDatabaseConnection getConnection() {
		return (OracleDatabaseConnection) super.getConnection();
	}

}
