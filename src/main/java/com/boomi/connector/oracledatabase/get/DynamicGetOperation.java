// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.get;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Map;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;
import com.boomi.connector.oracledatabase.util.CustomPayloadUtil;
import com.boomi.connector.oracledatabase.util.CustomResponseUtil;
import com.boomi.connector.oracledatabase.util.MetadataUtil;
import com.boomi.connector.util.SizeLimitedUpdateOperation;
import com.boomi.util.IOUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * The Class DynamicGetOperation.
 *
 * @author swastik.vn
 */
public class DynamicGetOperation extends SizeLimitedUpdateOperation {

	/**
	 * Instantiates a new dynamic get operation.
	 *
	 * @param con the con
	 */
	public DynamicGetOperation(OracleDatabaseConnection con) {
		super(con);
	}

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(DynamicGetOperation.class.getName());

	/**
	 * Execute size limited update.
	 *
	 * @param request  the request
	 * @param response the response
	 */
	@Override
	public void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		OracleDatabaseConnection conn = getConnection();
		Long maxRows = getContext().getOperationProperties().getLongProperty(MAX_ROWS);
		String linkElement = getContext().getOperationProperties().getProperty(LINK_ELEMENT);
		Long maxFieldSize = getContext().getOperationProperties().getLongProperty("maxFieldSize");
		try (Connection con = conn.getSoloConnection().connect(conn.getUrl(), conn.loadProperties())) {
			this.executeStatements(con, request, response, maxRows, linkElement, maxFieldSize);
		} catch (Exception e) {
			ResponseUtil.addExceptionFailures(response, request, e);
		}
	}

	/**
	 * Method which will fetch the maxRows linkElement from Operation UI and
	 * dynamically creates the Select Statements.
	 *
	 * @param con          the con
	 * @param trackedData  the tracked data
	 * @param response     the response
	 * @param maxRows      the max rows
	 * @param linkElement  the link element
	 * @param maxFieldSize the max field size
	 * @throws SQLException the SQL exception
	 */
	private void executeStatements(Connection con, UpdateRequest trackedData, OperationResponse response, Long maxRows,
			String linkElement, Long maxFieldSize) throws SQLException {

		Map<String, String> dataTypes = new MetadataUtil(con, getContext().getObjectTypeId()).getDataType();
		for (ObjectData objdata : trackedData) {
			PreparedStatement st = null;
			try {
				StringBuilder query = this.buildQuery(objdata, dataTypes, con);
				if (linkElement != null && !linkElement.equalsIgnoreCase("")) {
					this.addLinkElement(query, linkElement);
				}
				st = con.prepareStatement(query.toString());
				if (objdata.getDataSize() != 0) {
					this.prepareStatement(objdata, dataTypes, st, con);
				}
				if (maxRows != null && maxRows > 0) {
					st.setMaxRows(maxRows.intValue());
				}
				if (maxFieldSize != null && maxFieldSize > 0) {
					st.setMaxFieldSize(maxFieldSize.intValue());
				}
				this.processResultSet(st, objdata, response, con);
			} catch (SQLException e) {
				CustomResponseUtil.writeSqlErrorResponse(e, objdata, response);
			} catch (IOException e) {
				CustomResponseUtil.writeErrorResponse(e, objdata, response);
			} catch (ConnectorException e) {
				ResponseUtil.addExceptionFailure(response, objdata, e);
			} finally {
				if (st != null) {
					st.close();
				}
			}
		}
		logger.info("Statements proccessed Successfully!!");
	}

	/**
	 * This method will add the parameters to the Prepared Statements based on the
	 * incoming requests.
	 *
	 * @param userData  the user data
	 * @param dataTypes the data types
	 * @param pstmnt    the pstmnt
	 * @throws SQLException the SQL exception
	 */
	private void prepareStatement(ObjectData objdata, Map<String, String> dataTypes, PreparedStatement bstmnt,
			Connection con) throws IOException, SQLException {

		ObjectMapper mapper = new ObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)
				.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
		try (InputStream is = objdata.getData()) {
			JsonNode json = mapper.readTree(is);
			if (json != null) {
				DatabaseMetaData md = con.getMetaData();
				try (ResultSet resultSet = md.getColumns(null, null, getContext().getObjectTypeId(), null)) {
					int i = 0;
					while (resultSet.next()) {

						String key = resultSet.getString(COLUMN_NAME);
						JsonNode fieldName = json.get(key);
						if (dataTypes.containsKey(key) && fieldName != null) {
							i++;
							switch (dataTypes.get(key)) {
							case INTEGER:
								int num = Integer.parseInt(fieldName.toString().replace(BACKSLASH, ""));
								bstmnt.setInt(i, num);
								break;
							case DATE:
								bstmnt.setString(i, fieldName.toString().replace(BACKSLASH, ""));
								break;
							case STRING:
								String varchar = fieldName.toString().replace(BACKSLASH, "");
								bstmnt.setString(i, varchar);

								break;
							case TIME:
								String time = fieldName.toString().replace(BACKSLASH, "");
								bstmnt.setTime(i, Time.valueOf(time));
								break;
							case BOOLEAN:
								boolean flag = Boolean.parseBoolean(fieldName.toString().replace(BACKSLASH, ""));
								bstmnt.setBoolean(i, flag);
								break;
							default:
								break;
							}
						}
					}
				}
			} else {
				throw new ConnectorException("Please check the input data!!");
			}
		}

	}

	/**
	 * Builds the Prepared Statement by taking the request.
	 *
	 * @param objdata   the is
	 * @param dataTypes the data types
	 * @param con       the con
	 * @return the string builder
	 * @throws IOException  Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	private StringBuilder buildQuery(ObjectData objdata, Map<String, String> dataTypes, Connection con)
			throws IOException, SQLException {
		StringBuilder query = new StringBuilder(SELECT_INITIAL + getContext().getObjectTypeId());
		this.buildFinalQuery(con, query, objdata, dataTypes);
		return query;
	}

	/**
	 * Adds the link element field values to the GROUPBY Clause of the query.
	 *
	 * @param query       the query
	 * @param linkElement the link element
	 */
	private void addLinkElement(StringBuilder query, String linkElement) {
		query.append(GROUP_BY).append(linkElement);
	}

	/**
	 * This method will Process the Result Set and create the payload for Operation
	 * response.
	 *
	 * @param st       the st
	 * @param objdata  the objdata
	 * @param response the response
	 * @param con
	 */
	private void processResultSet(PreparedStatement st, ObjectData objdata, OperationResponse response,
			Connection con) {
		CustomPayloadUtil load = null;
		try (ResultSet rs = st.executeQuery()) {
			while (rs.next()) {
				load = new CustomPayloadUtil(rs, con);
				response.addPartialResult(objdata, OperationStatus.SUCCESS, SUCCESS_RESPONSE_CODE,
						SUCCESS_RESPONSE_MESSAGE, load);
			}
			response.finishPartialResult(objdata);

		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		} finally {
			IOUtil.closeQuietly(load);
		}

	}

	/**
	 * This Method will build the Sql query based on the request parameters.
	 *
	 * @param con       the con
	 * @param query     the query
	 * @param objdata
	 * @param is        the is
	 * @param dataTypes the data types
	 * @throws IOException  Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	private void buildFinalQuery(Connection con, StringBuilder query, ObjectData objdata, Map<String, String> dataTypes)
			throws IOException, SQLException {
		ObjectMapper mapper = new ObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS);
		try (InputStream is = objdata.getData()) {
			if (is.available() != 0) {
				// After filtering out the inputs (which are more than 1MB) we are loading the
				// inputstream to memory here.
				JsonNode json = mapper.readTree(is);
				if (json != null) {
					query.append(WHERE);
					boolean and = false;
					DatabaseMetaData md = con.getMetaData();
					try (ResultSet resultSet = md.getColumns(null, null, getContext().getObjectTypeId(), null)) {
						while (resultSet.next()) {
							String key = resultSet.getString(COLUMN_NAME);
							JsonNode node = json.get(key);
							if (node != null) {
								this.checkforAnd(and, query);
								query.append(key).append('=');
								if (dataTypes.containsKey(key)) {
									query.append('?');
								}
								and = true;
							}
						}
					}
				}

				else {
					throw new ConnectorException("Please check the Input Request!!!");
				}

			}
		}

	}

	/**
	 * This method will check whether the incoming request parameter is first one
	 * and append the AND character to the query accordingly.
	 *
	 * @param and   the and
	 * @param query the query
	 */
	private void checkforAnd(boolean and, StringBuilder query) {
		if (and) {
			query.append(" AND ");
		}

	}

	/**
	 * Gets the Connection instance.
	 *
	 * @return the connection
	 */
	@Override
	public OracleDatabaseConnection getConnection() {
		return (OracleDatabaseConnection) super.getConnection();
	}

}
