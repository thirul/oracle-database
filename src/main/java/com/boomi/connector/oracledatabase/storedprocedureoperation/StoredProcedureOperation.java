// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.storedprocedureoperation;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.BATCH_COUNT;

import java.sql.Connection;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;
import com.boomi.connector.util.SizeLimitedUpdateOperation;

/**
 * @author swastik.vn
 *
 */
public class StoredProcedureOperation extends SizeLimitedUpdateOperation {

	public StoredProcedureOperation(OracleDatabaseConnection conn) {
		super(conn);
	}

	@Override
	public void executeSizeLimitedUpdate(UpdateRequest request, OperationResponse response) {
		OracleDatabaseConnection conn = getConnection();
		String procedureName = getContext().getObjectTypeId();
		try (Connection con = conn.getSoloConnection().connect(conn.getUrl(), conn.loadProperties());) {
			con.setAutoCommit(false);
			Long batchCount = getContext().getOperationProperties().getLongProperty(BATCH_COUNT);
			Long maxFieldSize = getContext().getOperationProperties().getLongProperty("maxFieldSize");
			StoredProcedureExecute execute = new StoredProcedureExecute(con, procedureName, request, response, getContext());
			execute.executeStatements(batchCount, maxFieldSize);
		} catch (Exception e) {
			ResponseUtil.addExceptionFailures(response, request, e);
		}
	
	}

	/**
	 * Gets the Connection instance
	 */
	@Override
	public OracleDatabaseConnection getConnection() {
		return (OracleDatabaseConnection) super.getConnection();
	}
}