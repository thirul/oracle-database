// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.storedprocedureoperation;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.BatchUpdateException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.JsonPayloadUtil;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.model.BatchResponse;
import com.boomi.connector.oracledatabase.model.ErrorDetails;
import com.boomi.connector.oracledatabase.model.ProcedureResponseNonBatch;
import com.boomi.connector.oracledatabase.util.CustomPayloadUtil;
import com.boomi.connector.oracledatabase.util.CustomResponseUtil;
import com.boomi.connector.oracledatabase.util.OracleDatabaseConstants;
import com.boomi.connector.oracledatabase.util.ProcedureMetaDataUtil;
import com.boomi.connector.oracledatabase.util.QueryBuilderUtil;
import com.boomi.util.IOUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.Datum;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import oracle.xdb.XMLType;

/**
 * The Class StoredProcedureExecute.
 *
 * @author swastik.vn
 */
public class StoredProcedureExecute extends ProcedureMetaDataUtil {

	/** The List of Parameters present in the procedure. */
	List<String> params;

	/** The List of only IN Parameters present in the procedure. */
	List<String> inParams;

	/** The List of only OUT parameters present in the procedure. */
	List<String> outParams;

	/** The data type. */
	Map<String, Integer> dataType;

	/** The tracked data. */
	UpdateRequest trackedData;

	/** The response. */
	OperationResponse response;

	/** The con. */
	Connection con;

	/** The procedure name. */
	String procedureName;

	/** The operation context. */
	OperationContext operationContext;
	
	/** The mapper. */
	ObjectMapper mapper = new ObjectMapper().disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS)
			.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

	/** The Constant JSON_FACTORY. */
	private static final JsonFactory JSON_FACTORY = new JsonFactory();

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(StoredProcedureExecute.class.getName());

	/**
	 * Instantiates a new stored procedure helper.
	 *
	 * @param con              the database Connection Object
	 * @param procedureName    the procedure name
	 * @param trackedData      the tracked data
	 * @param response         the response
	 * @param operationContext the operation context
	 */
	public StoredProcedureExecute(Connection con, String procedureName, UpdateRequest trackedData,
			OperationResponse response, OperationContext operationContext) {
		
		super(con, procedureName);
		this.params = getParams();
		this.inParams = getInParams();
		this.outParams = getOutParams();
		this.dataType = getDataType();
		this.trackedData = trackedData;
		this.response = response;
		this.con = con;
		this.procedureName = procedureName;
		this.operationContext = operationContext;

	}

	/**
	 * This method will create the Callable statement and provide the neccessary
	 * parameters and execute the statements.
	 *
	 * @param batchCount   the batch count
	 * @param maxFieldSize the max field size
	 * @throws SQLException            the SQL exception
	 */
	public void executeStatements(Long batchCount, Long maxFieldSize)
			throws SQLException {
		StringBuilder query = QueryBuilderUtil.buildProcedureQuery(params, procedureName);
		if (batchCount != null && batchCount > 0) {
			if (!inParams.isEmpty() && outParams.isEmpty()) {
				// We are extending SizeLimitUpdate Operation it loads only single document into
				// memory. Hence we are preparing the list of Object Data which will be required
				// for Statement batching.
				List<ObjectData> batchData = new ArrayList<>();
				for (ObjectData objdata : trackedData) {
					batchData.add(objdata);
				}
				this.doBatch(batchCount, query, batchData);
			} else {
				throw new ConnectorException("Batching cannot be applied for non input parameter procedures");
			}

		} else if (batchCount == null || batchCount == 0) {
			try (OracleCallableStatement csmt = (OracleCallableStatement) con.prepareCall(query.toString())) {
				this.registerParams(csmt);
				for (ObjectData objdata : trackedData) {
					try (InputStream is = objdata.getData();) {
						if (!inParams.isEmpty()) {
							this.prepareStatements(csmt, is);
						}
						this.callProcedure(csmt, objdata, maxFieldSize);
					} catch (IOException | IllegalArgumentException e) {
						CustomResponseUtil.writeErrorResponse(e, objdata, response);
					} catch (SQLException e) {
						CustomResponseUtil.writeSqlErrorResponse(e, objdata, response);
					} catch (ConnectorException e) {
						ResponseUtil.addExceptionFailure(response, objdata, e);
					}

				}
			}
			try {
				con.commit();
			} catch (SQLException e) {
				throw new ConnectorException(e.getMessage());
			}

		} else if (batchCount < 0) {
			throw new ConnectorException("Batch count cannot be negative!!");
		}

	}

	/**
	 * This method will register the out parameters for the Callable statement
	 *
	 * @param csmt the Callable Statement
	 * @throws SQLException the SQL exception
	 */
	private void registerParams(OracleCallableStatement csmt) throws SQLException {
		
		if (!params.isEmpty()) {
			for (int i = 1; i <= params.size(); i++) {
				if (outParams.contains(params.get(i - 1))
						&& dataType.get(params.get(i - 1)).equals(2012)) {
					csmt.registerOutParameter(params.indexOf(params.get(i - 1)) + 1,
							OracleTypes.CURSOR);
				} else if (outParams.contains(params.get(i - 1))
						&& (dataType.get(params.get(i - 1)).equals(2003)
								|| dataType.get(params.get(i - 1)).equals(2002))) {
					String typeName = QueryBuilderUtil.getTypeName(con, procedureName,
							params.get(i - 1));
					csmt.registerOutParameter(params.indexOf(params.get(i - 1)) + 1, 2003, typeName);
				} else if (outParams.contains(params.get(i - 1))
						&& dataType.get(params.get(i - 1)).equals(2009)) {
					csmt.registerOutParameter(params.indexOf(params.get(i - 1)) + 1, Types.SQLXML);
				} else if (outParams.contains(params.get(i - 1))
						&& !dataType.get(params.get(i - 1)).equals(2003)
						&& !dataType.get(params.get(i - 1)).equals(2012)) {
					csmt.registerOutParameter(params.indexOf(params.get(i - 1)) + 1,
							dataType.get(params.get(i - 1)));
				}
			}
		}
		
	}

	/**
	 * Do batch.
	 *
	 * @param batchCount the batch count
	 * @param query      the query
	 * @param batchData  the batch data
	 * @throws SQLException   the SQL exception
	 */
	private void doBatch(Long batchCount, StringBuilder query, List<ObjectData> batchData)
			throws SQLException {
		int batchnum = 0;
		int b = 0;
		boolean shouldExecute = true;
		// Note: Here the CallableStatement will be held in the memory. This issue has
		// been addressed in dbv2 connector. We will be informing the user to use batch
		// count less than 10 to limit the memory been held for some extent.
		try (CallableStatement csmt = con.prepareCall(query.toString())) {
			for (ObjectData objdata : batchData) {
				Payload payload = null;
				b++;
				try (InputStream is = objdata.getData();) {
					this.prepareStatements((OracleCallableStatement) csmt, is);
					csmt.addBatch();
					if (b == batchCount) {
						batchnum++;
						if (shouldExecute) {
							int res[] = csmt.executeBatch();
							con.commit();
							csmt.clearBatch();
							response.getLogger().log(Level.INFO, OracleDatabaseConstants.BATCH_NUM + batchnum);
							response.getLogger().log(Level.INFO, OracleDatabaseConstants.BATCH_RECORDS + res.length);
							payload = JsonPayloadUtil
									.toPayload(new BatchResponse("Batch executed successfully", batchnum, res.length));
							ResponseUtil.addSuccess(response, objdata, OracleDatabaseConstants.SUCCESS_RESPONSE_CODE,
									payload);
						} else {
							csmt.clearBatch();
							shouldExecute = true;
							CustomResponseUtil.logFailedBatch(response, batchnum, b);
							CustomResponseUtil.batchExecuteError(objdata, response, batchnum, b);
						}
						b = 0;
					} else if (b < batchCount) {
						int remainingBatch = batchnum + 1;
						if (batchData.lastIndexOf(objdata) == batchData.size() - 1) {
							this.executeRemaining(objdata, csmt, remainingBatch, b);
						} else {
							payload = JsonPayloadUtil.toPayload(
									new BatchResponse("Record added to batch successfully", remainingBatch, b));
							ResponseUtil.addSuccess(response, objdata, OracleDatabaseConstants.SUCCESS_RESPONSE_CODE,
									payload);
						}

					}
				} catch (BatchUpdateException e) {
					CustomResponseUtil.logFailedBatch(response, batchnum, b);
					CustomResponseUtil.batchExecuteError(objdata, response, batchnum, b);
					b = 0;
				} catch (SQLException e) {
					CustomResponseUtil.logFailedBatch(response, batchnum, b);
					shouldExecute = this.checkLastRecord(b, batchCount);
					if (shouldExecute) {
						b = 0;
					}
					CustomResponseUtil.writeSqlErrorResponse(e, objdata, response);
				} catch (IOException | IllegalArgumentException e) {
					shouldExecute = this.checkLastRecord(b, batchCount);
					if (shouldExecute || batchData.lastIndexOf(objdata) == batchData.size() - 1) {
						csmt.clearBatch();
						batchnum++;
						CustomResponseUtil.logFailedBatch(response, batchnum, b);
						b = 0;
					}
					CustomResponseUtil.writeErrorResponse(e, objdata, response);
				} finally {
					IOUtil.closeQuietly(payload);
				}
			}
		}

	}

	/**
	 * This method will provide the necessary parameters required for the Callable
	 * statement based on the incoming requests.
	 *
	 * @param csmt  the Callable Statement
	 * @param is    the input(inputstream)
	 * @throws SQLException   the SQL exception
	 * @throws IOException    Signals that an I/O exception has occurred.
	 */
	private void prepareStatements(OracleCallableStatement csmt, InputStream is)
			throws SQLException, IOException {
		
		JsonNode json = null;
		if (is.available() != 0) {
			// After filtering out the inputs (which are more than 1MB) we are loading the
			json = mapper.readTree(is);
			for (int i = 1; i <= json.size(); i++) {
				JsonNode node = json.get(inParams.get(i - 1));
				switch (dataType.get(inParams.get(i - 1))) {
				case Types.VARCHAR:
				case Types.DATE:
				case Types.TIMESTAMP:
				case Types.CHAR:
					if (node == null || node.toString().replace(BACKSLASH, "").equals("")) {
						csmt.setNull(params.indexOf(inParams.get(i - 1)) + 1, Types.VARCHAR);
					} else {
						csmt.setString(params.indexOf(inParams.get(i - 1)) + 1, node.toString().replace("\"", ""));
					}
					break;
				case Types.CLOB:
					if (node == null || node.toString().replace(BACKSLASH, "").equals("")) {
						csmt.setNull(params.indexOf(inParams.get(i - 1)) + 1, Types.CLOB);
					} else {
						csmt.setString(params.indexOf(inParams.get(i - 1)) + 1, node.toString().replace("\"", ""));
					}
					break;
				case Types.INTEGER:
				case Types.NUMERIC:
					if (node == null || node.toString().replace(BACKSLASH, "").equals("")) {
						csmt.setNull(params.indexOf(inParams.get(i - 1)) + 1, Types.NUMERIC);
					} else {
						csmt.setInt(params.indexOf(inParams.get(i - 1)) + 1,
								Integer.valueOf(node.toString().replace(BACKSLASH, "")));
					}
					break;
				case Types.TIME:
					if (node == null || node.toString().replace(BACKSLASH, "").equals("")) {
						csmt.setNull(params.indexOf(inParams.get(i - 1)) + 1, Types.TIME);
					} else {
						csmt.setTime(params.indexOf(inParams.get(i - 1)) + 1,
								Time.valueOf(node.toString().replace(BACKSLASH, "")));
					}
					break;
				case Types.BOOLEAN:
					if (node == null || node.toString().replace(BACKSLASH, "").equals("")) {
						csmt.setNull(params.indexOf(inParams.get(i - 1)) + 1, Types.CHAR);
					} else {
						csmt.setBoolean(params.indexOf(inParams.get(i - 1)) + 1,
								Boolean.valueOf(node.toString().replace(BACKSLASH, "")));
					}
					break;
				case Types.ARRAY:
					ARRAY array = this.getArrayType(con, inParams.get(i - 1), node);
					csmt.setArray(params.indexOf(inParams.get(i - 1)) + 1, array);
					break;
				case Types.SQLXML:
					if (node == null || node.toString().replace(BACKSLASH, "").equals("")) {
						csmt.setNull(params.indexOf(inParams.get(i - 1)) + 1, Types.SQLXML, "XMLTYPE");
					} else {
						String xml = node.toString().replace(BACKSLASH, "");
						String jsonSt = unEscapeString(xml);
						XMLType x = XMLType.createXML(con, jsonSt);
						csmt.setSQLXML(params.indexOf(inParams.get(i - 1)) + 1, x);
					}
					break;
				case Types.STRUCT:
					csmt.setARRAY(params.indexOf(inParams.get(i - 1)) + 1, this.getStructData(node, inParams.get(i - 1)));
					break;
				default:
					break;
				}
			}

		}

	}

	
	/**
	 * This method will build the Struct data required for table type parameters in Stored procedure from the Json node.
	 *
	 * @param node Json input
	 * @param argument parameter name of the SP
	 * @return the ARRAY
	 * @throws SQLException the SQL exception
	 */
	private ARRAY getStructData(JsonNode node, String argument) throws SQLException {

		STRUCT[] structArray = new STRUCT[node.size()];
		ArrayDescriptor arrayDesc = ArrayDescriptor
				.createDescriptor(QueryBuilderUtil.getTypeName(con, procedureName, argument), con);
		StructDescriptor structDesc = StructDescriptor.createDescriptor(arrayDesc.getBaseName(), con);
		ResultSetMetaData md = structDesc.getMetaData();
		for (int i = 0; i < node.size(); i++) {
			JsonNode child = node.get(i);
			Object[] obj = new Object[md.getColumnCount()];
			for (int j = 1; j<=md.getColumnCount(); j++) {
				if(child.get(md.getColumnName(j)) == null || child.get(md.getColumnName(j)).toString().equals("null") || child.get(md.getColumnName(j)).toString().replace(BACKSLASH, "").equals("")) {
					obj[j-1] = null;
				} else {
				switch (md.getColumnType(j)) {
				case Types.NUMERIC:
				case Types.INTEGER:
					obj[j-1] = Integer.valueOf(child.get(md.getColumnName(j)).toString().replace(BACKSLASH, ""));
					break;
				case Types.BOOLEAN:
					obj[j-1] = Boolean.valueOf(child.get(md.getColumnName(j)).toString().replace(BACKSLASH, ""));
					break;
				case Types.DATE:
				case Types.TIMESTAMP:
				case Types.VARCHAR:
					obj[j-1] = child.get(md.getColumnName(j)).toString().replace(BACKSLASH, "");
					break;
				default:
					break;
				}
			}
			}
			structArray[i] = new STRUCT(structDesc, con, obj);
		}
		return new ARRAY(arrayDesc, con, structArray);
	}

	/**
	 * This method will enscape the characters from the xml string and prepare the input accordingly.
	 *
	 * @param s the input xml string
	 * @return the escaped string
	 */
	public static String unEscapeString(String s) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++)
			switch (s.charAt(i)) {
			case '\n':
				sb.append("\\n");
				break;
			case '\"':
				sb.append("\\\"");
				break;
			case '\\':
				sb.append("\"");
				break;
			// ... rest of escape characters
			default:
				sb.append(s.charAt(i));
			}
		return sb.toString();
	}

	/**
	 * Gets the Array Object for inserting into prepared statenents.
	 *
	 * @param con       the java.sql.Connection
	 * @param key       the param name
	 * @param fieldName the value
	 * @return the ARRAY
	 * @throws SQLException the SQL exception
	 */
	private ARRAY getArrayType(Connection con, String key, JsonNode fieldName) throws SQLException {

		String typeName = QueryBuilderUtil.getTypeName(con, procedureName, key);
		ArrayDescriptor des = ArrayDescriptor.createDescriptor(typeName, con);

		Object[] array = new Object[fieldName.size()];
		int k = 0;
		for (int j = 1; j <= fieldName.size(); j++) {
			JsonNode elements = fieldName.get(ELEMENT + j);
			if (elements != null) {
				if (elements.toString().replace("\"", "").equals("")) {
					array[k] = null;
				} else {
					array[k] = elements.toString().replace("\"", "");
				}
				k++;
			}
		}
		return new ARRAY(des, con, array);

	}



	/**
	 * Method that will execute remaining records in the batch.
	 *
	 * @param objdata  the objdata
	 * @param csmt     the csmt
	 * @param batchnum the batchnum
	 * @param b        the b
	 */
	private void executeRemaining(ObjectData objdata, CallableStatement csmt, int batchnum, int b) {

		Payload payload = null;
		try {
			int res[] = csmt.executeBatch();
			response.getLogger().log(Level.INFO, OracleDatabaseConstants.BATCH_NUM + batchnum);
			response.getLogger().log(Level.INFO, OracleDatabaseConstants.REMAINING_BATCH_RECORDS + res.length);
			payload = JsonPayloadUtil.toPayload(new BatchResponse(
					"Remaining records added to batch and executed successfully", batchnum, res.length));
			ResponseUtil.addSuccess(response, objdata, OracleDatabaseConstants.SUCCESS_RESPONSE_CODE, payload);
			con.commit();

		} catch (SQLException e) {

			CustomResponseUtil.logFailedBatch(response, batchnum, b);
			CustomResponseUtil.writeSqlErrorResponse(e, objdata, response);

		} finally {
			IOUtil.closeQuietly(payload);
		}

	}

	/**
	 * This method will check whether the input is the last object data of the batch
	 * or not.
	 *
	 * @param b          the b
	 * @param batchCount the batch count
	 * @return if yes returns true or else return false
	 */

	private boolean checkLastRecord(int b, Long batchCount) {
		return b == batchCount;
	}

	/**
	 * This method will call the procedure and process the resultset based on the
	 * OUT param.
	 *
	 * @param csmt         the Callable Statement
	 * @param objdata      the objdata
	 * @param maxFieldSize the max field size
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	private void callProcedure(CallableStatement csmt, ObjectData objdata, Long maxFieldSize)
			throws SQLException, IOException {
		if (maxFieldSize != null && maxFieldSize > 0) {
			csmt.setMaxFieldSize(maxFieldSize.intValue());
		}
		boolean result = csmt.execute();
		this.processOutParams(csmt, result, objdata);
	}

	/**
	 * This method will process the Out params based on the OUT Param Datatypes and
	 * values.
	 *
	 * @param csmt    the Callable Statement
	 * @param result  boolean to detrmine whether executed procedure has resulset
	 * @param objdata the objdata
	 * @throws SQLException the SQL exception
	 * @throws IOException  Signals that an I/O exception has occurred.
	 */
	private void processOutParams(CallableStatement csmt, boolean result, ObjectData objdata)
			throws SQLException, IOException {
		InputStream tempInputStream = null;
		OutputStream out = null;
		// temporary outputStream to flush the content of JsonGenerator.
		out = operationContext.createTempOutputStream();
		JsonGenerator generator = JSON_FACTORY.createGenerator(out);
		try (ResultSet rs = csmt.getResultSet();) {
			if (outParams != null && !outParams.isEmpty() && !result) {
				generator.writeStartObject();
				for (int i = 0; i <= outParams.size() - 1; i++) {
					if (dataType.get(outParams.get(i)) == 2003) {
						this.processVarrays(generator, csmt, i);
					} else if (dataType.get(outParams.get(i)).equals(2012)) {
						this.processRefCursors(generator, csmt, i);
					} 
					else if (dataType.get(outParams.get(i)).equals(2002)) {
						this.processStruct(generator, (OracleCallableStatement) csmt, params.indexOf(outParams.get(i)) + 1, outParams.get(i));
					}
					else if (dataType.get(outParams.get(i)).equals(2009)) {
						if(csmt.getSQLXML(params.indexOf(outParams.get(i)) + 1) == null) {
							generator.writeNullField(outParams.get(i));
						}else {
							generator.writeStringField(outParams.get(i),
									csmt.getSQLXML(params.indexOf(outParams.get(i)) + 1).getString());
						}
						generator.flush();
					}
					else if (null == csmt.getString(params.indexOf(outParams.get(i)) + 1)
							|| csmt.getString(params.indexOf(outParams.get(i)) + 1).isEmpty()) {
						generator.writeNullField(outParams.get(i));
						generator.flush();
					} else if (null != csmt.getString(params.indexOf(outParams.get(i)) + 1)
							|| !csmt.getString(params.indexOf(outParams.get(i)) + 1).isEmpty()) {
						generator.writeStringField(outParams.get(i),
								csmt.getString(params.indexOf(outParams.get(i)) + 1));
						generator.flush();
					} 
					else {
						response.addResult(objdata, OperationStatus.APPLICATION_ERROR,
								OracleDatabaseConstants.SUCCESS_RESPONSE_CODE,
								OracleDatabaseConstants.SUCCESS_RESPONSE_MESSAGE,
								JsonPayloadUtil.toPayload(new ErrorDetails(405, "Value from Out parameter is Null")));
					}
				}
				generator.writeEndObject();
				generator.flush();
				tempInputStream = operationContext.tempOutputStreamToInputStream(out);
				response.addResult(objdata, OperationStatus.SUCCESS, OracleDatabaseConstants.SUCCESS_RESPONSE_CODE,
						OracleDatabaseConstants.SUCCESS_RESPONSE_MESSAGE, ResponseUtil.toPayload(tempInputStream));
			} else if (result && rs != null) {
				this.processResultset(objdata, rs);

			} else if (!result && rs == null) {
				response.addResult(objdata, OperationStatus.SUCCESS, OracleDatabaseConstants.SUCCESS_RESPONSE_CODE,
						OracleDatabaseConstants.SUCCESS_RESPONSE_MESSAGE, JsonPayloadUtil
								.toPayload(new ProcedureResponseNonBatch(200, "Procedure Executed Successfully!!")));
			}
			logger.info("Procedure called Successfully!!!");
		} finally {
			IOUtil.closeQuietly(tempInputStream, out, generator);
		}

	}

	/**
	 * Method to process STRUCT Data for table type parameters in Stored Procedure.
	 *
	 * @param generator the generator
	 * @param csmt the csmt
	 * @param i the i
	 * @param outParam the out param
	 * @throws SQLException the SQL exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processStruct(JsonGenerator generator, OracleCallableStatement csmt, int i, String outParam)
			throws SQLException, IOException {
		ARRAY array = csmt.getARRAY(i);
		ArrayDescriptor arrayDescriptor = new ArrayDescriptor(
				QueryBuilderUtil.getTypeName(con, procedureName, outParam), con);
		StructDescriptor structDescriptor = StructDescriptor.createDescriptor(arrayDescriptor.getBaseName(), con);
		Datum[] data = array.getOracleArray();
		ResultSetMetaData md = structDescriptor.getMetaData();
		generator.writeArrayFieldStart(outParam);
		for (int j = 0; j < data.length; j++) {
			generator.writeStartObject();
			Object[] elements = ((STRUCT) data[j]).getAttributes();
			int k = 1;
			for (Object element : elements) {
				if (element == null) {
					generator.writeNullField(md.getColumnName(k++));
				} else if (element instanceof BigDecimal) {
					generator.writeNumberField(md.getColumnName(k++),
							((BigDecimal) element).intValue());
				} else if (element instanceof String) {
					generator.writeStringField(md.getColumnName(k++), (String) element);
				} else if (element instanceof Timestamp) {
					generator.writeStringField(md.getColumnName(k++), element.toString());
				}
			}
			generator.writeEndObject();
		}
		generator.writeEndArray();
	}

	/**
	 * This method will process OUT params of type REFCURSORS. This method will add
	 * the values from the refcursors to the JsonGenerator and flush each column
	 * value content to the temporary OutputStream.
	 *
	 * @param generator the generator
	 * @param csmt      the csmt
	 * @param i         the i
	 * @throws SQLException the SQL exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processRefCursors(JsonGenerator generator, CallableStatement csmt, int i)
			throws SQLException, IOException {
		try (ResultSet set = ((OracleCallableStatement) csmt).getCursor(params.indexOf(outParams.get(i)) + 1)) {
			generator.writeArrayFieldStart(outParams.get(i));
			while (set.next()) {
				generator.writeStartObject();
				for (int l = 1; l <= set.getMetaData().getColumnCount(); l++) {
					switch (set.getMetaData().getColumnType(l)) {
					case Types.INTEGER:
					case Types.NUMERIC:
						generator.writeNumberField(set.getMetaData().getColumnName(l),
								set.getInt(set.getMetaData().getColumnName(l)));
						break;
					case Types.VARCHAR:
					case Types.CLOB:
					case Types.DATE:
					case Types.TIME:
					case Types.TIMESTAMP:
						generator.writeStringField(set.getMetaData().getColumnName(l),
								set.getString(set.getMetaData().getColumnName(l)));
						break;
					case Types.BOOLEAN:
						generator.writeBooleanField(set.getMetaData().getColumnName(l),
								set.getBoolean(set.getMetaData().getColumnName(l)));
						break;
					default:
						break;
					}
					generator.flush();
				}
				generator.writeEndObject();
			}
			generator.writeEndArray();
			generator.flush();
		}
	}

	/**
	 * This method will process VARRAYS from the OUT Params and add the values to
	 * JsonGenerator based on the datatypes.
	 *
	 * @param generator the generator
	 * @param csmt      the csmt
	 * @param i         the i
	 * @throws IOException  Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	private void processVarrays(JsonGenerator generator, CallableStatement csmt, int i)
			throws IOException, SQLException {
		generator.writeFieldName(outParams.get(i));
		generator.writeStartObject();
		if (csmt.getArray(params.indexOf(outParams.get(i)) + 1) != null) {
			Array arr = csmt.getArray(params.indexOf(outParams.get(i)) + 1);
			switch (arr.getBaseType()) {
			case 2:
				BigDecimal[] nos = (BigDecimal[]) arr.getArray();
				for (int j = 1; j <= nos.length; j++) {
					generator.writeNumberField(ELEMENT + j, nos[j - 1]);
				}
				break;
			case 12:
				String[] chars = (String[]) arr.getArray();
				for (int j = 1; j <= chars.length; j++) {
					if (null == chars[j - 1]) {
						generator.writeNullField(ELEMENT + j);
					} else {
						generator.writeStringField(ELEMENT + j, chars[j - 1]);
					}
				}
				break;
			case 91:
				Timestamp[] date = (Timestamp[]) arr.getArray();
				for (int j = 1; j <= date.length; j++) {
					if (null == date[j - 1]) {
						generator.writeNullField(ELEMENT + j);
					} else {
						generator.writeStringField(ELEMENT + j, date[j - 1].toString());
					}
				}
				break;

			default:
				break;
			}

		}
		generator.writeEndObject();
		generator.flush();

	}

	/**
	 * This method will process the resultset and Writes each field from the
	 * resultset to the payload.
	 *
	 * @param objdata the objdata
	 * @param rs      the rs
	 */
	private void processResultset(ObjectData objdata, ResultSet rs) {

		CustomPayloadUtil load = null;
		try {
			while (rs.next()) {
				load = new CustomPayloadUtil(rs);
				response.addPartialResult(objdata, OperationStatus.SUCCESS,
						OracleDatabaseConstants.SUCCESS_RESPONSE_CODE, OracleDatabaseConstants.SUCCESS_RESPONSE_MESSAGE,
						load);
			}
			response.finishPartialResult(objdata);
		} catch (SQLException e) {
			throw new ConnectorException(e.toString());
		} finally {
			IOUtil.closeQuietly(load);
		}

	}

}
