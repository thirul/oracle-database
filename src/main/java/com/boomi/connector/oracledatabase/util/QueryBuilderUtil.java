// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.util;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import com.boomi.connector.api.ConnectorException;
import com.fasterxml.jackson.databind.JsonNode;

import oracle.sql.ArrayDescriptor;
import oracle.sql.StructDescriptor;

/**
 * The Class QueryBuilderUtil. This class is used to generate Dynamic Prepared
 * Statements based on the request for all the Dynamic Operations
 *
 * @author swastik.vn
 */
public class QueryBuilderUtil {

	/**
	 * Instantiates a new query builder util.
	 */
	private QueryBuilderUtil() {

	}

	/**
	 * Utility method to build the Query for procedure call.
	 *
	 * @param params     the params
	 * @param objectType the object type
	 * @return the string builder
	 */
	public static StringBuilder buildProcedureQuery(List<String> params, String objectType) {
		StringBuilder query = new StringBuilder("call ");
		query.append(objectType);
		query.append('(');
		if (!params.isEmpty()) {
			for (int i = 1; i <= params.size(); i++) {
				query.append(PARAM);
			}
			query.deleteCharAt(query.length() - 1);
		}
		query.append(')');

		return query;

	}

	/**
	 * This method will build the initial characters required for the insert query.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @return query
	 * @throws SQLException the SQL exception
	 */
	public static StringBuilder buildInitialQuery(Connection con, String objectTypeId) throws SQLException {
		StringBuilder query = new StringBuilder(QUERY_INITIAL);
		query.append(objectTypeId);
		query.append('(');
		DatabaseMetaData md = con.getMetaData();
		try (ResultSet resultSet = md.getColumns(null, null, objectTypeId, null)) {
			while (resultSet.next()) {
				query.append(resultSet.getString(COLUMN_NAME));
				query.append(',');
			}
		}
		query.deleteCharAt(query.length() - 1);
		query.append(QUERY_VALUES);
		return query;

	}

	/**
	 * Builds the Query required for UPSERT Operation which will make use of dual
	 * table in Oracle database.
	 *
	 * @param objectTypeId the object type id
	 * @param pk           the pk
	 * @param dataTypes    the data types
	 * @return the string builder
	 * @throws SQLException the SQL exception
	 */
	public static StringBuilder buildUpsertQuery(String objectTypeId, String pk, Map<String, String> dataTypes) {
		StringBuilder query = new StringBuilder("MERGE INTO ");
		query.append(objectTypeId).append(" USING DUAL ON (").append(pk).append(" = ?) WHEN MATCHED THEN UPDATE SET ");
		for (Map.Entry<String, String> columns : dataTypes.entrySet()) {
			if (!columns.getKey().equals(pk)) {
				query.append(columns.getKey());
				query.append("=?,");
			}
		}
		query.deleteCharAt(query.length() - 1);
		query.append("WHEN NOT MATCHED THEN INSERT (");
		for (Map.Entry<String, String> columns : dataTypes.entrySet()) {
			query.append(columns.getKey() + ',');
		}
		query.deleteCharAt(query.length() - 1);
		query.append(QUERY_VALUES);
		for (int i = 1; i <= dataTypes.size(); i++) {
			query.append(PARAM);
		}
		query.deleteCharAt(query.length() - 1);
		query.append(')');

		return query;

	}

	/**
	 * Builds the Insert Query Required of Prepared Statement in Dynamic Insert.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @return the string builder
	 * @throws SQLException the SQL exception
	 */
	public static StringBuilder buildInitialPstmntInsert(Connection con, String objectTypeId) throws SQLException {
		StringBuilder query = new StringBuilder(QUERY_INITIAL);
		query.append(objectTypeId).append('(');
		DatabaseMetaData md = con.getMetaData();
		boolean nestedTable = false;
		int paramCount = 0;
		try (ResultSet resultSet = md.getColumns(null, null, objectTypeId, null)) {
			while (resultSet.next()) {
				paramCount++;
				query.append(resultSet.getString(COLUMN_NAME));
				query.append(',');
				if (resultSet.getString(DATA_TYPE).equals(ARRAY_TYPE)
						|| resultSet.getString(DATA_TYPE).equals(NESTED_TABLE)) {
					nestedTable = true;
				}
			}
		}
		query.deleteCharAt(query.length() - 1);
		query.append(QUERY_VALUES);
		boolean removeComma = true;
		boolean varray = false;
		if (nestedTable) {
			removeComma = false;
			boolean removeCommaInnerTable = true;
			try (ResultSet resultSet = md.getColumns(null, null, objectTypeId, null)) {
				while (resultSet.next()) {
					if (resultSet.getString(DATA_TYPE).equals(ARRAY_TYPE)
							|| resultSet.getString(DATA_TYPE).equals(NESTED_TABLE)) {

						ArrayDescriptor arraydes1 = ArrayDescriptor.createDescriptor(
								con.getMetaData().getUserName() + DOT + resultSet.getString(TYPE_NAME), con);
						if (arraydes1.getBaseName().equals(NUMBER) || arraydes1.getBaseName().equals(VARCHAR)
								|| arraydes1.getBaseName().equalsIgnoreCase(DATE)) {
							varray = true;
							query.append(PARAM);
						} else {
							varray = false;
							query.append(resultSet.getString(TYPE_NAME));
							query.append('(').append(
									arraydes1.getBaseName().replaceAll(con.getMetaData().getUserName() + DOT, ""))
									.append('(');
							StructDescriptor struct1 = StructDescriptor.createDescriptor(arraydes1.getBaseName(), con);
							ResultSetMetaData rsmd1 = struct1.getMetaData();
							for (int j = 1; j <= rsmd1.getColumnCount(); j++) {
								if (rsmd1.getColumnType(j) == 2003) {
									removeCommaInnerTable = false;
									query.append(rsmd1.getColumnTypeName(j)
											.replaceAll(con.getMetaData().getUserName() + DOT, "") + '(');
									ArrayDescriptor arraydes2 = ArrayDescriptor
											.createDescriptor(rsmd1.getColumnTypeName(j), con);
									query.append(arraydes2.getBaseName()
											.replaceAll(con.getMetaData().getUserName() + DOT, "") + '(');
									StructDescriptor struct2 = StructDescriptor
											.createDescriptor(arraydes2.getBaseName(), con);
									ResultSetMetaData rsmd2 = struct2.getMetaData();
									for (int k = 1; k <= rsmd2.getColumnCount(); k++) {
										if (rsmd2.getColumnType(k) == 2003) {
											ArrayDescriptor arraydes3 = ArrayDescriptor
													.createDescriptor(rsmd2.getColumnTypeName(j), con);
											if (arraydes3.getBaseName().equals(NUMBER)
													|| arraydes3.getBaseName().equals(VARCHAR)
													|| arraydes3.getBaseName().equalsIgnoreCase(DATE)) {
												query.append(PARAM);
											} else {
												throw new ConnectorException("Nested table level exhasted!!!");
											}
										} else {
											query.append(PARAM);
										}
									}
									query.deleteCharAt(query.length() - 1);
									query.append("))");
								} else {
									query.append(PARAM);
								}

							}
							if (removeCommaInnerTable) {
								query.deleteCharAt(query.length() - 1);
							}
							query.append("))");

						}
					} else {
						query.append(PARAM);
					}
					if (removeComma) {
						query.deleteCharAt(query.length() - 1);
					}

				}
			}
		} else {
			for (int i = 1; i <= paramCount; i++) {
				query.append(PARAM);
			}
		}
		if (removeComma || varray) {
			query.deleteCharAt(query.length() - 1);
		}
		query.append(')');
		return query;

	}

	/**
	 * Gets the type name of the given parameter of the procedure.
	 *
	 * @param con           the con
	 * @param procedureName the procedure name
	 * @param aurgumentName the aurgument name
	 * @return the type name
	 * @throws SQLException the SQL exception
	 */
	public static String getTypeName(Connection con, String procedureName, String aurgumentName) throws SQLException {
		ResultSet rs = null;
		String typeName = null;
		StringBuilder query = new StringBuilder(
				"SELECT TYPE_NAME, DATA_TYPE FROM SYS.ALL_ARGUMENTS WHERE OBJECT_NAME = ? AND ARGUMENT_NAME = ?");
		try (PreparedStatement psmnt = con.prepareStatement(query.toString())) {
			psmnt.setString(1, procedureName.toUpperCase());
			psmnt.setString(2, aurgumentName.toUpperCase());
			rs = psmnt.executeQuery();
			rs.next();
			typeName = rs.getString(OracleDatabaseConstants.TYPE_NAME);
		} finally {
			if (rs != null) {
				rs.close();
			}
		}
		return typeName;
	}

	/**
	 * This method will check the Datatype of the key and append the JsonNode values
	 * to the prepared statement accordingly.
	 *
	 * @param bstmnt    the bstmnt
	 * @param dataTypes the data types
	 * @param key       the key
	 * @param fieldName the field name
	 * @param i         the i
	 * @throws SQLException the SQL exception
	 */
	public static void checkDataType(PreparedStatement bstmnt, Map<String, String> dataTypes, String key,
			JsonNode fieldName, int i) throws SQLException {

		switch (dataTypes.get(key)) {
		case INTEGER:
			if (fieldName != null) {
				int num = Integer.parseInt(fieldName.toString().replace(BACKSLASH, ""));
				bstmnt.setInt(i, num);
			} else {
				bstmnt.setNull(i, Types.INTEGER);
			}
			break;
		case DATE:
			if (fieldName != null) {
				bstmnt.setString(i, fieldName.toString().replace(BACKSLASH, ""));
			} else {
				bstmnt.setNull(i, Types.DATE);
			}
			break;
		case STRING:
			if (fieldName != null) {
				bstmnt.setString(i, fieldName.toString().replace(BACKSLASH, ""));
			} else {
				bstmnt.setNull(i, Types.VARCHAR);
			}
			break;
		case TIME:
			if (fieldName != null) {
				bstmnt.setTime(i, Time.valueOf(fieldName.toString().replace(BACKSLASH, "")));
			} else {
				bstmnt.setNull(i, Types.TIME);
			}
			break;
		case BOOLEAN:
			if (fieldName != null) {
				boolean flag = Boolean.parseBoolean(fieldName.toString().replace(BACKSLASH, ""));
				bstmnt.setBoolean(i, flag);
			} else {
				bstmnt.setNull(i, Types.BOOLEAN);
			}
			break;
		default:
			break;
		}

	}

	/**
	 * This method will check the Datatype of the key and append the values to the
	 * prepared statement accordingly.
	 *
	 * @param bstmnt    the bstmnt
	 * @param dataTypes the data types
	 * @param key       the key
	 * @param fieldName the field name
	 * @param i         the i
	 * @throws SQLException the SQL exception
	 */
	public static void checkDataType(PreparedStatement bstmnt, Map<String, String> dataTypes, String key,
			String fieldName, int i) throws SQLException {

		switch (dataTypes.get(key)) {
		case INTEGER:
			if (fieldName != null) {
				int num = Integer.parseInt(fieldName);
				bstmnt.setInt(i, num);
			} else {
				bstmnt.setNull(i, Types.INTEGER);
			}
			break;
		case DATE:
			if (fieldName != null) {
				bstmnt.setString(i, fieldName);
			} else {
				bstmnt.setNull(i, Types.DATE);
			}
			break;
		case STRING:
			if (fieldName != null) {
				bstmnt.setString(i, fieldName);
			} else {
				bstmnt.setNull(i, Types.VARCHAR);
			}
			break;
		case TIME:
			if (fieldName != null) {
				bstmnt.setTime(i, Time.valueOf(fieldName));
			} else {
				bstmnt.setNull(i, Types.TIME);
			}
			break;
		case BOOLEAN:
			if (fieldName != null) {
				boolean flag = Boolean.parseBoolean(fieldName);
				bstmnt.setBoolean(i, flag);
			} else {
				bstmnt.setNull(i, Types.BOOLEAN);
			}
			break;
		default:
			break;
		}

	}

	/**
	 * This method will check the Datatype of the key and append the values to the
	 * prepared statement accordingly.
	 *
	 * @param pstmnt    the pstmnt
	 * @param dataTypes the data types
	 * @param key       the key
	 * @param entry     the entry
	 * @param i         the i
	 * @throws SQLException the SQL exception
	 */
	public static void checkDataType(PreparedStatement pstmnt, Map<String, String> dataTypes, String key,
			Map.Entry<String, Object> entry, int i) throws SQLException {
		switch (dataTypes.get(key)) {
		case INTEGER:
			int num = (Integer) entry.getValue();
			pstmnt.setInt(i, num);
			break;
		case STRING:
			String varchar = (String) entry.getValue();
			pstmnt.setString(i, varchar);
			break;
		case DATE:
			String date = (String) entry.getValue();
			pstmnt.setString(i, date);
			break;
		case TIME:
			String time = (String) entry.getValue();
			pstmnt.setTime(i, Time.valueOf(time));
			break;
		case BOOLEAN:
			Boolean flag = (Boolean) entry.getValue();
			pstmnt.setBoolean(i, flag);
			break;
		default:
			break;
		}
	}

	/**
	 * Validate the table name.
	 *
	 * @param finalQuery the final query
	 * @return the string
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	public static String validateTheTableName(String finalQuery) {
		int findIndex = finalQuery.lastIndexOf("FROM");
		String tep = finalQuery.substring(findIndex);
		if (tep.indexOf(" ") >= 0) {
			tep = tep.substring(tep.indexOf(" ") + 1);
			if (tep.indexOf(" ") >= 0)
				finalQuery = tep.substring(0, tep.indexOf(" "));
			else
				finalQuery = tep;
		}
		if (finalQuery.contains(".")) {
			finalQuery = finalQuery.substring(finalQuery.indexOf(DOT) + 1, finalQuery.length());
		}
		return finalQuery;
	}

}
