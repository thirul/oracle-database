// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.util;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.boomi.connector.api.ConnectorException;

import oracle.sql.ArrayDescriptor;
import oracle.sql.StructDescriptor;

/**
 * The Class MetadataUtil.
 *
 * @author swastik.vn
 */
public class MetadataUtil {
	
	/** The data types. */
	private Map<String, String> dataTypes = new HashMap<>();
	
	/** The type names. */
	private Map<String, String> typeNames = new LinkedHashMap<>();

	/**
	 * Instantiates a new metadata util.
	 *
	 * @param con the con
	 * @param objectTypeId the object type id
	 */
	public MetadataUtil(Connection con, String objectTypeId) {
			this.dataTypes = this.getDataTypes(con, objectTypeId);
	}

	/** The Constant VARCHAR. */
	public static final String VARCHAR = "12";

	/** The Constant CLOB. */
	public static final String CLOB = "2005";

	/** The Constant LONGVARCHAR. */
	public static final String LONGVARCHAR = "-1";

	/** The Constant INTEGER. */
	public static final String INTEGER = "4";

	/** The Constant NUMERIC. */
	public static final String NUMERIC = "2";

	/** The Constant TIMESTAMP. */
	public static final String TIMESTAMP = "93";

	/** The Constant DATE. */
	public static final String DATE = "91";

	/** The Constant TINYINT. */
	public static final String TINYINT = "-6";

	/** The Constant BOOLEAN. */
	public static final String BOOLEAN = "16";

	/** The Constant BIT. */
	public static final String BIT = "-7";

	/** The Constant TIME. */
	public static final String TIME = "92";

	/** The Constant CHAR. */
	public static final String CHAR = "1";
	
	/** The Constant ARRAY. */
	public static final String ARRAY_TYPE = "2003";

	/**
	 * This method will fetch the dataTypes of the columns and stores it in a Map.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @return type
	 */
	public Map<String, String> getDataTypes(Connection con, String objectTypeId) {
		Map<String, String> dataType = new HashMap<>();
		try (ResultSet resultSet = con.getMetaData().getColumns(null, null, objectTypeId, null)) {
			while (resultSet.next()) {
				switch (resultSet.getString(DATA_TYPE)) {
				case VARCHAR:
				case CLOB:
				case LONGVARCHAR:
				case CHAR:
					dataType.put(resultSet.getString(COLUMN_NAME), STRING);
					break;
				case INTEGER:
				case NUMERIC:
					dataType.put(resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.INTEGER);
					break;
				case DATE:
				case TIMESTAMP:
					dataType.put(resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.DATE);
					break;
				case TINYINT:
				case BOOLEAN:
				case BIT:
					dataType.put(resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.BOOLEAN);
					break;
				case TIME:
					dataType.put(resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.TIME);
					break;
				case ARRAY_TYPE:
					ArrayDescriptor array = ArrayDescriptor.createDescriptor(resultSet.getString(TYPE_NAME), con);
					if (array.getBaseName().equals(NUMBER) || array.getBaseName().equals(OracleDatabaseConstants.VARCHAR)
							|| array.getBaseName().equalsIgnoreCase(OracleDatabaseConstants.DATE)) {
						dataType.put(resultSet.getString(COLUMN_NAME), ARRAY);
					} else {
						iterateOverNestedTable(dataType, con, resultSet);
					}
					break;
				default:
					break;
				}
				typeNames.put(resultSet.getString(COLUMN_NAME), resultSet.getString(TYPE_NAME));
			}
		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}
		return dataType;
	}

	/**
	 * This method will fetch the dataTypes of the columns and stores it in a Map.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @return type
	 * @throws SQLException the SQL exception
	 */
	public static Map<String, String> getDataTypesWithTable(Connection con, String objectTypeId) throws SQLException {

		Map<String, String> type = new HashMap<>();
		DatabaseMetaData md = con.getMetaData();
		try (ResultSet resultSet = md.getColumns(null, null, objectTypeId, null);) {
			while (resultSet.next()) {
				switch (resultSet.getString(DATA_TYPE)) {
				case VARCHAR:
				case CLOB:
				case LONGVARCHAR:
				case CHAR:
					type.put(objectTypeId + DOT +resultSet.getString(COLUMN_NAME), STRING);
					break;
				case INTEGER:
				case NUMERIC:
					type.put(objectTypeId + DOT +resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.INTEGER);
					break;
				case DATE:
				case TIMESTAMP:
					type.put(objectTypeId + DOT +resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.DATE);
					break;
				case TINYINT:
				case BOOLEAN:
				case BIT:
					type.put(objectTypeId + DOT +resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.BOOLEAN);
					break;
				case TIME:
					type.put(objectTypeId + DOT +resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.TIME);
					break;
				case ARRAY_TYPE:
					ArrayDescriptor array = ArrayDescriptor.createDescriptor(resultSet.getString(TYPE_NAME), con);
					if (array.getBaseName().equals(NUMBER) || array.getBaseName().equals(OracleDatabaseConstants.VARCHAR)
							|| array.getBaseName().equalsIgnoreCase(OracleDatabaseConstants.DATE)) {
						type.put(objectTypeId + DOT +resultSet.getString(COLUMN_NAME), ARRAY);
					} else {
						iterateOverNestedTable(type, con, resultSet);
					}
					break;
				default:
					break;
				}
			}
		}
		return type;
	}

	/**
	 * This method will iterate over nested table and get the metadata of the inner
	 * tables.
	 *
	 * @param type      the type
	 * @param con       the con
	 * @param resultSet the result set
	 * @throws SQLException the SQL exception
	 */
	private static void iterateOverNestedTable(Map<String, String> type, Connection con, ResultSet resultSet)
			throws SQLException {

		type.put(resultSet.getString(COLUMN_NAME), OracleDatabaseConstants.ARRAY);
		ArrayDescriptor arrayLevel1 = ArrayDescriptor
				.createDescriptor(con.getMetaData().getUserName() + "." + resultSet.getString(TYPE_NAME), con);
		StructDescriptor structLevel1 = StructDescriptor.createDescriptor(arrayLevel1.getBaseName(), con);
		ResultSetMetaData rsmd1 = structLevel1.getMetaData();
		for (int j = 1; j <= rsmd1.getColumnCount(); j++) {

			if (rsmd1.getColumnType(j) == 2) {
				type.put(rsmd1.getColumnName(j), OracleDatabaseConstants.INTEGER);
			} else if (rsmd1.getColumnType(j) == 91) {
				type.put(rsmd1.getColumnName(j), OracleDatabaseConstants.DATE);
			} else if (rsmd1.getColumnType(j) == 12 || rsmd1.getColumnType(j) == 1) {
				type.put(rsmd1.getColumnName(j), OracleDatabaseConstants.STRING);
			} else if (rsmd1.getColumnType(j) == 2003) {
				ArrayDescriptor arrayLevel2 = ArrayDescriptor.createDescriptor(rsmd1.getColumnTypeName(j), con);
				if (arrayLevel2.getBaseName().equals(NUMBER)
						|| arrayLevel2.getBaseName().equals(OracleDatabaseConstants.VARCHAR)
						|| arrayLevel2.getBaseName().equalsIgnoreCase(OracleDatabaseConstants.DATE)) {
					type.put(rsmd1.getColumnName(j), ARRAY);
				} else {
					type.put(rsmd1.getColumnName(j), ARRAY);
					StructDescriptor structLevel2 = StructDescriptor.createDescriptor(arrayLevel2.getBaseName(), con);
					ResultSetMetaData rsmd2 = structLevel2.getMetaData();
					for (int k = 1; k <= rsmd2.getColumnCount(); k++) {
						if (rsmd2.getColumnType(k) == 2) {
							type.put(rsmd2.getColumnName(k), OracleDatabaseConstants.INTEGER);
						} else if (rsmd2.getColumnType(k) == 91) {
							type.put(rsmd2.getColumnName(k), OracleDatabaseConstants.DATE);
						} else if (rsmd2.getColumnType(k) == 12 || rsmd2.getColumnType(k) == 1) {
							type.put(rsmd2.getColumnName(k), OracleDatabaseConstants.STRING);
						} else if (rsmd2.getColumnType(k) == 2003) {
							ArrayDescriptor arrayLevel3 = ArrayDescriptor.createDescriptor(rsmd2.getColumnTypeName(k),
									con);
							if (arrayLevel3.getBaseName().equals(NUMBER)
									|| arrayLevel3.getBaseName().equals(OracleDatabaseConstants.VARCHAR)
									|| arrayLevel3.getBaseName().equalsIgnoreCase(OracleDatabaseConstants.DATE)) {
								type.put(rsmd2.getColumnName(k), ARRAY);
							} else {
								throw new ConnectorException("Nested level exhausted!!");
							}

						}
					}

				}

			}
		}
	}
	
	/**
	 * Gets the list of primary keys.
	 *
	 * @param con the con
	 * @param objectTypeId the object type id
	 * @return the primary key
	 * @throws SQLException the SQL exception
	 */
	public static String getPrimaryKey(Connection con, String objectTypeId) throws SQLException {
		List<String> pk = new ArrayList<>();
		try (ResultSet resultSet = con.getMetaData().getPrimaryKeys(null, con.getSchema(), objectTypeId.toUpperCase())) {
			while (resultSet.next()) {
				if (resultSet.getString(COLUMN_NAME) != null)
					pk.add(resultSet.getString(COLUMN_NAME));
			}
		}
		if (pk.size() > 1) {
			throw new ConnectorException(
					"Dynamic Upsert Operation is not applicable for table having more than one primary key/composite keys. Please select Standard Upsert.");
		}
		return pk.get(0).isEmpty() ? "" : pk.get(0);

	}
	
	/**
	 * Gets the data type.
	 *
	 * @return the data type
	 */
	public Map<String, String> getDataType() {
		return dataTypes;
	}

	/**
	 * Gets the type names.
	 *
	 * @return the type names
	 */
	public Map<String, String> getTypeNames() {
		return typeNames;
	}
}
