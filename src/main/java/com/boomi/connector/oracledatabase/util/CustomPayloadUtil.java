// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.util;

import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.DATE;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.ELEMENT;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.NUMBER;
import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.VARCHAR;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.boomi.connector.api.BasePayload;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ExtendedPayload;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;

/**
 * The Class CustomPayloadUtil.
 *
 * @author swastik.vn
 */
public class CustomPayloadUtil extends BasePayload implements ExtendedPayload {

	/** The Constant JSON_FACTORY. */
	private static final JsonFactory JSON_FACTORY = new JsonFactory();

	/** The rs. */
	private ResultSet rs;

	/** The generator. */
	JsonGenerator generator = null;

	/** The con. */
	Connection con;

	/**
	 * Creates a new instance. Closing the payload will close the Resultset
	 *
	 * @param resultset the resultset
	 */

	public CustomPayloadUtil(ResultSet resultset) {
		this.rs = resultset;
	}

	/**
	 * Instantiates a new custom payload util with Connection Object.
	 *
	 * @param rs  the rs
	 * @param con the con
	 */
	public CustomPayloadUtil(ResultSet rs, Connection con) {
		this.rs = rs;
		this.con = con;
	}

	/**
	 * This method will write the resultset in Json using JsonGenerator to Output
	 * stream.
	 *
	 * @param out OutputStream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void writeTo(OutputStream out) throws IOException {
		generator = JSON_FACTORY.createGenerator(out);
		try {
			generator.writeStartObject();

			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				if (rs.getMetaData().getColumnType(i) == 4 || rs.getMetaData().getColumnType(i) == 2) {
					int value = rs.getInt(rs.getMetaData().getColumnLabel(i));
					generator.writeNumberField(rs.getMetaData().getColumnLabel(i), value);
					generator.flush();
				}
				if (rs.getMetaData().getColumnType(i) == 12 || rs.getMetaData().getColumnType(i) == 91
						|| rs.getMetaData().getColumnType(i) == 92 || rs.getMetaData().getColumnType(i) == -1
						|| rs.getMetaData().getColumnType(i) == 2005 || rs.getMetaData().getColumnType(i) == 93) {
					String varchar = rs.getString(rs.getMetaData().getColumnLabel(i));
					generator.writeStringField(rs.getMetaData().getColumnLabel(i), varchar);
					generator.flush();
				}
				if (rs.getMetaData().getColumnType(i) == 16 || rs.getMetaData().getColumnType(i) == -7) {
					boolean flag = rs.getBoolean(rs.getMetaData().getColumnLabel(i));
					generator.writeBooleanField(rs.getMetaData().getColumnLabel(i), flag);
					generator.flush();
				}
				if (rs.getMetaData().getColumnType(i) == 2003) {
					this.writeArrayField(i);
				}

			}
			generator.writeEndObject();
			generator.flush();
		} catch (SQLException e) {
			throw new ConnectException(e.getMessage());
		} finally {
			generator.close();
		}
	}

	/**
	 * Method for writing array field to Json Generator.
	 *
	 * @param i the i
	 * @throws IOException  Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	private void writeArrayField(int i) throws IOException, SQLException {

		ArrayDescriptor desc = ArrayDescriptor.createDescriptor(rs.getMetaData().getColumnTypeName(i), con);
		if (desc.getBaseName().equals(NUMBER) || desc.getBaseName().equals(VARCHAR)
				|| desc.getBaseName().equalsIgnoreCase(DATE)) {
			generator.writeFieldName(rs.getMetaData().getColumnLabel(i));
			generator.writeStartObject();
			Array charar = rs.getArray(i);
			if (desc.getBaseName().equals(VARCHAR)) {
				String[] values = (String[]) charar.getArray();
				for (int j = 1; j <= values.length; j++) {
					int k = j - 1;
					if (null != values[k]) {
						generator.writeStringField(OracleDatabaseConstants.ELEMENT + j, values[k]);
					} else {
						generator.writeNullField(OracleDatabaseConstants.ELEMENT + j);
					}

				}
			} else if (desc.getBaseName().equals(NUMBER)) {
				BigDecimal[] values = (BigDecimal[]) charar.getArray();
				for (int j = 1; j <= values.length; j++) {
					int k = j - 1;
					if (null != values[k]) {
						generator.writeNumberField(ELEMENT + j, values[k]);
					} else {
						generator.writeNullField(ELEMENT + j);
					}

				}
			} else if (desc.getBaseName().equalsIgnoreCase(DATE)) {
				Timestamp[] values = (Timestamp[]) charar.getArray();
				for (int j = 1; j <= values.length; j++) {
					int k = j - 1;
					if (null != values[k]) {
						generator.writeStringField(ELEMENT + j, values[k].toString());
					} else {
						generator.writeNullField(ELEMENT + j);
					}

				}
			}
			generator.flush();
			generator.writeEndObject();
		} else {
			this.writeNestedTableValues(i);

		}

	}

	/**
	 * Write nested table values to Json Generator.
	 *
	 * @param i the i
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	private void writeNestedTableValues(int i) throws IOException, SQLException {

		generator.writeFieldName(rs.getMetaData().getColumnLabel(i));
		generator.writeStartObject();
		Object[] data = (Object[]) ((Array) rs.getObject(i)).getArray();
		for (Object element : data) {
			STRUCT rowLevel1 = (STRUCT) element;
			StructDescriptor structLevel1 = rowLevel1.getDescriptor();
			int k = 0;
			for (Object attribute : rowLevel1.getAttributes()) {
				k++;
				if (attribute instanceof oracle.sql.ARRAY) {
					Object[] obj = (Object[]) ((oracle.sql.ARRAY) attribute).getArray();
					STRUCT rowLevel2 = (STRUCT) obj[0];
					StructDescriptor structLevel2 = rowLevel2.getDescriptor();
					generator.writeFieldName(structLevel1.getMetaData().getColumnName(k));
					generator.writeStartObject();
					int l = 0;
					for (Object attribute2 : rowLevel2.getAttributes()) {
						l++;
						if (structLevel2.getMetaData().getColumnType(l) == 12
								|| structLevel2.getMetaData().getColumnType(l) == 91) {
							generator.writeStringField(structLevel2.getMetaData().getColumnName(l),
									(String) attribute2);
						} else if (structLevel2.getMetaData().getColumnType(l) == 2) {
							generator.writeNumberField(structLevel2.getMetaData().getColumnName(l),
									(BigDecimal) attribute2);
						} else if (structLevel2.getMetaData().getColumnType(l) == 2003) {
							ArrayDescriptor desc2 = ArrayDescriptor
									.createDescriptor(structLevel2.getMetaData().getColumnTypeName(l), con);
							generator.writeFieldName(structLevel2.getMetaData().getColumnName(l));
							generator.writeStartObject();
							if (desc2.getBaseName().equals(VARCHAR)) {
								String[] values = (String[]) ((oracle.sql.ARRAY) attribute2).getArray();
								for (int j = 1; j <= values.length; j++) {
									int n = j - 1;
									if (null != values[n]) {
										generator.writeStringField(ELEMENT + j, values[n]);
									} else {
										generator.writeNullField(ELEMENT + j);
									}
								}
							} else if (desc2.getBaseName().equals(NUMBER)) {
								BigDecimal[] values = (BigDecimal[]) ((oracle.sql.ARRAY) attribute2).getArray();
								for (int j = 1; j <= values.length; j++) {
									int n = j - 1;
									if (null != values[n]) {
										generator.writeNumberField(ELEMENT + j, values[n]);
									} else {
										generator.writeNullField(ELEMENT + j);
									}
								}
							} else if (desc2.getBaseName().equalsIgnoreCase(DATE)) {
								Timestamp[] values = (Timestamp[]) ((oracle.sql.ARRAY) attribute2).getArray();
								for (int j = 1; j <= values.length; j++) {
									int n = j - 1;
									if (null != values[n]) {
										generator.writeStringField(ELEMENT + j, values[n].toString());
									} else {
										generator.writeNullField(ELEMENT + j);
									}
								}
							}
							generator.flush();
							generator.writeEndObject();
						}

					}
					generator.writeEndObject();
				} else if (attribute instanceof oracle.sql.NUMBER) {
					generator.writeNumberField(structLevel1.getMetaData().getColumnName(k), (BigDecimal) attribute);
				} else {
					generator.writeObjectField(structLevel1.getMetaData().getColumnName(k), attribute);
				}

			}

		}
		generator.writeEndObject();
	}

	/**
	 * Close.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void close() throws IOException {
		try {
			if (!generator.isClosed()) {
				generator.close();
			}

		} catch (Exception e) {
			throw new ConnectorException(e.getMessage());
		}
	}
	

}
