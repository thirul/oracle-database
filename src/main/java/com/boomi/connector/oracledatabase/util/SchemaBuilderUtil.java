// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.oracledatabase.util;


import static com.boomi.connector.oracledatabase.util.OracleDatabaseConstants.*;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.oracledatabase.model.DeletePojo;
import com.boomi.connector.oracledatabase.model.QueryResponse;
import com.boomi.connector.oracledatabase.model.UpdatePojo;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.factories.SchemaFactoryWrapper;

import oracle.sql.ArrayDescriptor;
import oracle.sql.StructDescriptor;

/**
 * This Util Class will Generate the JSON Schema by taking each column name from
 * the table and associated DataTypes of the column. Based on the column
 * datatype the Schema Type will be either String, Boolean or Number.
 * 
 * @author swastik.vn
 *
 */
public class SchemaBuilderUtil {

	/**
	 * Instantiates a new schema builder util.
	 */
	private SchemaBuilderUtil() {

	}

	/**
	 * Build Json Schema Based on the Database Column names and column types.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @param enableQuery  the enable query
	 * @param output the output
	 * @return the json schema
	 */
	public static String getJsonSchema(Connection con, String objectTypeId, boolean enableQuery, boolean output) {

		String jsonSchema = null;
		StringBuilder sbSchema = new StringBuilder();
		Map<String, String> dataTypes = new HashMap<>();
		String[] tableNames = getTableNames(objectTypeId);
		try {
			if (objectTypeId.contains(",")) {
				for (String tableName : tableNames) {
					dataTypes.putAll(MetadataUtil.getDataTypesWithTable(con, tableName.trim()));
				}
			} else {
				dataTypes.putAll(new MetadataUtil(con, objectTypeId).getDataType());
			}
			DatabaseMetaData md = con.getMetaData();
			sbSchema.append("{").append(JSON_DRAFT4_DEFINITION).append(" \"").append(JSONUtil.SCHEMA_TYPE)
			.append(OBJECT_STRING).append(" \"").append(JSONUtil.SCHEMA_PROPERTIES).append(OPEN_PROPERTIES);
			for (String tableName : tableNames) {
				try (ResultSet resultSet = md.getColumns(null, null, tableName.trim(), null);) {
					while (resultSet.next()) {
						String param = "";
						if (objectTypeId.contains(",")) {
							param = tableName.trim() + DOT + resultSet.getString(COLUMN_NAME);
						} else {
							param = resultSet.getString(COLUMN_NAME);
						}
						if (output) {
							sbSchema.append("\"" + resultSet.getString(COLUMN_NAME) + OPEN_PROPERTIES);
						} else {
							sbSchema.append("\"" + param + OPEN_PROPERTIES);
						}
						if (!dataTypes.containsKey(param) && !dataTypes.containsKey(resultSet.getString(COLUMN_NAME))) {
							throw new SQLException("The data type " + resultSet.getString(TYPE_NAME)
									+ " is not supported in the connector!");
						}
						if (STRING.equals(dataTypes.get(param)) || TIME.equals(dataTypes.get(param))
								|| DATE.equals(dataTypes.get(param))) {
							sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
						} else if (INTEGER.equals(dataTypes.get(param))) {
							sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
						} else if (BOOLEAN.equals(dataTypes.get(param))) {
							sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + BOOLEAN + BACKSLASH);
						} else {
							// array parameter starts
							ArrayDescriptor array = ArrayDescriptor
									.createDescriptor(resultSet.getString(OracleDatabaseConstants.TYPE_NAME), con);
							sbSchema.append(TYPE_OBJECT);
							sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
							if (array.getBaseName().equals(NUMBER) || array.getBaseName().equals(VARCHAR)
									|| array.getBaseName().equalsIgnoreCase(DATE)) {
								iterateOverVarrays(array, sbSchema);
							} else {
								iterateOverNestedTables(array, sbSchema, con);
							}
							sbSchema.deleteCharAt(sbSchema.length() - 1);
							sbSchema.append('}');
							// array parameter ends
						}
						sbSchema.append("},");
					}
					if (enableQuery) {
						sbSchema.append(BACKSLASH + SQL_QUERY + OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
						sbSchema.append("},");
					}
				}
			}
		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}
		sbSchema.deleteCharAt(sbSchema.length() - 1);
		sbSchema.append("}}");
		JsonNode rootNode = null;
		try {
			rootNode = JSONUtil.getDefaultObjectMapper().readTree( sbSchema.toString());
			if (rootNode != null) {
				jsonSchema = JSONUtil.prettyPrintJSON(rootNode);
			}
		} catch (Exception e) {
			throw new ConnectorException(SCHEMA_BUILDER_EXCEPTION, e.getMessage());
		}
		return jsonSchema;
	}



	/**
	 * This method will generate array type of schema for IN Clause
	 *
	 * @param con the con
	 * @param objectTypeId the object type id
	 * @param enableQuery the enable query
	 * @return the json array schema
	 */
	public static String getJsonArraySchema(Connection con, String objectTypeId, boolean enableQuery) {
		String jsonSchema = null;
		StringBuilder sbSchema = new StringBuilder();
		Map<String, String> dataTypes = new HashMap<>();
		String[] tableNames = getTableNames(objectTypeId);
		try {
			if (objectTypeId.contains(",")) {
				for (String tableName : tableNames) {
					dataTypes.putAll(MetadataUtil.getDataTypesWithTable(con, tableName.trim()));
				}
			} else {
				dataTypes.putAll(new MetadataUtil(con, objectTypeId).getDataType());
			}
			DatabaseMetaData md = con.getMetaData();
			sbSchema.append("{").append(JSON_DRAFT4_DEFINITION).append(" \"").append(JSONUtil.SCHEMA_TYPE)
			.append(OBJECT_STRING).append(" \"").append(JSONUtil.SCHEMA_PROPERTIES).append(OPEN_PROPERTIES);
			for (String tableName : tableNames) {
				try (ResultSet resultSet = md.getColumns(null, null, tableName.trim(), null);) {
					while (resultSet.next()) {
						String param = "";
						if (objectTypeId.contains(",")) {
							param = tableName.trim() + DOT + resultSet.getString(COLUMN_NAME);
						} else {
							param = resultSet.getString(COLUMN_NAME);
						}
						sbSchema.append(BACKSLASH + param + OPEN_PROPERTIES);
						if (STRING.equals(dataTypes.get(param)) || TIME.equals(dataTypes.get(param))
								|| DATE.equals(dataTypes.get(param))) {
							sbSchema.append(OPEN_ARRAY).append(OPEN_ITEMS).append("\"type\": \"string\"\r\n").append(CLOSE_ARRAY);
						} else if (INTEGER.equals(dataTypes.get(param))) {
							sbSchema.append(OPEN_ARRAY).append(OPEN_ITEMS).append("\"type\": \"integer\"\r\n").append(CLOSE_ARRAY);
						} else if (BOOLEAN.equals(dataTypes.get(param))) {
							sbSchema.append(OPEN_ARRAY).append(OPEN_ITEMS).append("\"type\": \"boolean\"\r\n").append(CLOSE_ARRAY);
						} else {
							// array parameter starts
							ArrayDescriptor array = ArrayDescriptor
									.createDescriptor(resultSet.getString(OracleDatabaseConstants.TYPE_NAME), con);
							sbSchema.append(TYPE_OBJECT);
							sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);

							if (array.getBaseName().equals(NUMBER) || array.getBaseName().equals(VARCHAR)
									|| array.getBaseName().equalsIgnoreCase(DATE)) {
								iterateOverVarrays(array, sbSchema);
							} else {
								iterateOverNestedTables(array, sbSchema, con);
							}
							sbSchema.deleteCharAt(sbSchema.length() - 1);
							sbSchema.append('}');
							// array parameter ends
						}
						sbSchema.append("},");
					}
					if (enableQuery) {
						sbSchema.append(BACKSLASH).append(SQL_QUERY).append(OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH).append(JSONUtil.SCHEMA_TYPE).append(DOUBLE_BACKSLASH).append(STRING).append(BACKSLASH);
						sbSchema.append("},");
					}
				}
			}
		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}
		sbSchema.deleteCharAt(sbSchema.length() - 1);
		sbSchema.append("}}");
		JsonNode rootNode = null;
		try {
			rootNode = JSONUtil.getDefaultObjectMapper().readTree(sbSchema.toString());
			if (rootNode != null) {
				jsonSchema = JSONUtil.prettyPrintJSON(rootNode);
			}
		} catch (Exception e) {
			throw new ConnectorException(SCHEMA_BUILDER_EXCEPTION, e.getMessage());
		}
		return jsonSchema;
	}

	/**
	 * This Method will build the Json Schema for all the Standard Operations.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @param enableQuery  the enable query
	 * @return the standard json schema
	 */
	public static String getStandardJsonSchema(Connection con, String objectTypeId, boolean enableQuery) {

		String jsonSchema = null;
		StringBuilder sbSchema = new StringBuilder();
		Map<String, String> dataTypes;
		try {
			dataTypes = new MetadataUtil(con, objectTypeId).getDataType();
			DatabaseMetaData md = con.getMetaData();
			sbSchema.append("{").append(JSON_DRAFT4_DEFINITION).append(" \"").append(JSONUtil.SCHEMA_TYPE)
			.append(OBJECT_STRING).append(" \"").append(JSONUtil.SCHEMA_PROPERTIES).append(OPEN_PROPERTIES);
			try (ResultSet resultSet = md.getColumns(null, null, objectTypeId, null)) {
				if (enableQuery) {
					sbSchema.append(BACKSLASH + SQL_QUERY + OPEN_PROPERTIES);
					sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
					sbSchema.append("},");
					enableQuery = false;
				}
				while (resultSet.next()) {
					String param = resultSet.getString(COLUMN_NAME);
					if (STRING.equals(dataTypes.get(param)) || TIME.equals(dataTypes.get(param))
							|| DATE.equals(dataTypes.get(param))) {
						sbSchema.append(BACKSLASH + param + OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
					} else if (INTEGER.equals(dataTypes.get(param))) {
						sbSchema.append(BACKSLASH + param + OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
					} else if (BOOLEAN.equals(dataTypes.get(param))) {
						sbSchema.append(BACKSLASH + param + OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + BOOLEAN + BACKSLASH);
					} else {
						ArrayDescriptor array = ArrayDescriptor
								.createDescriptor(resultSet.getString(OracleDatabaseConstants.TYPE_NAME), con);
						if (array.getBaseName().equals(NUMBER) || array.getBaseName().equals(VARCHAR)
								|| array.getBaseName().equalsIgnoreCase(DATE)) {
							sbSchema.append(BACKSLASH + param + OPEN_PROPERTIES);
							sbSchema.append(TYPE_OBJECT);
							sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
							iterateOverVarrays(array, sbSchema);
							sbSchema.deleteCharAt(sbSchema.length() - 1);
							sbSchema.append('}');
						} else {
							iterateOverStandardNestedTable(array, sbSchema, con);
						}
					}
					sbSchema.append("},");
				}
			}

		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}
		sbSchema.deleteCharAt(sbSchema.length() - 1);
		sbSchema.append("}}");
		JsonNode rootNode = null;
		try {
			rootNode = JSONUtil.getDefaultObjectMapper().readTree(sbSchema.toString());
			if (rootNode != null) {
				jsonSchema = JSONUtil.prettyPrintJSON(rootNode);
			}
		} catch (Exception e) {
			throw new ConnectorException(SCHEMA_BUILDER_EXCEPTION, e.getMessage());
		}
		return jsonSchema;
	}

	/**
	 * This method will build the Json Schema for Stored Procedure based on the
	 * Input Parameters and its DataTypes.
	 *
	 * @param con          the con
	 * @param objectTypeId the object type id
	 * @param params the params
	 * @return the procedure schema
	 * @throws SQLException the SQL exception
	 */
	public static String getProcedureSchema(Connection con, String objectTypeId, List<String> params)
			throws SQLException {
		String jsonSchema = null;
		StringBuilder sbSchema = new StringBuilder();
		Map<String, Integer> dataTypes = null;
		dataTypes = new ProcedureMetaDataUtil(con, objectTypeId).getDataType();
		if (!params.isEmpty()) {
			sbSchema.append("{");
			sbSchema.append(JSON_DRAFT4_DEFINITION);
			sbSchema.append(" \"" + JSONUtil.SCHEMA_TYPE + OBJECT_STRING);
			sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
			for (String param : params) {
				sbSchema.append(BACKSLASH + param + OPEN_PROPERTIES);
				if (dataTypes.get(param).equals(12) || dataTypes.get(param).equals(92) || dataTypes.get(param).equals(93)
						|| dataTypes.get(param).equals(91) || dataTypes.get(param).equals(-1)
						|| dataTypes.get(param).equals(1) || dataTypes.get(param).equals(2005)
						|| dataTypes.get(param).equals(2009)) {
					sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
				} else if (dataTypes.get(param).equals(4) || dataTypes.get(param).equals(2)) {
					sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
				} else if (dataTypes.get(param).equals(16) || dataTypes.get(param).equals(-7)
						|| dataTypes.get(param).equals(-6)) {
					sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + BOOLEAN + BACKSLASH);
				} else if (dataTypes.get(param).equals(2003) || dataTypes.get(param).equals(2002)) {

						ArrayDescriptor array = ArrayDescriptor
								.createDescriptor(QueryBuilderUtil.getTypeName(con, objectTypeId, param), con);
						sbSchema.append("\"type\": \"object\",");
						sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);

						if(array.getBaseType() == 2002) {
							iterateOverNestedTables(array, sbSchema, con);
						}
						for (int i = 1; i <= array.getMaxLength(); i++) {
							sbSchema.append(BACKSLASH + ELEMENT + i + OPEN_PROPERTIES);
							if (array.getBaseName().equals(VARCHAR) || array.getBaseName().equalsIgnoreCase(DATE)) {
								sbSchema.append("\"type\": \"" + STRING + BACKSLASH);
							} else if (array.getBaseName().equals(NUMBER)) {
								sbSchema.append("\"type\": \"" + INTEGER + BACKSLASH);
							}
							sbSchema.append("},");
						}
						sbSchema.deleteCharAt(sbSchema.length() - 1);
						sbSchema.append('}');

				}
				sbSchema.append("},");
			}
			sbSchema.deleteCharAt(sbSchema.length() - 1);
			sbSchema.append("}}");
			JsonNode rootNode = null;
			try {
				rootNode = JSONUtil.getDefaultObjectMapper().readTree(sbSchema.toString());
				if (rootNode != null) {
					jsonSchema = JSONUtil.prettyPrintJSON(rootNode);
				}
			} catch (Exception e) {
				throw new ConnectorException(SCHEMA_BUILDER_EXCEPTION, e.getMessage());
			}
		}
		return jsonSchema;
	}

	/**
	 * This method will get the Json Schema for Dynamic Update, Stored procedure and
	 * Dynamic Delete Response.
	 *
	 * @param opsType the ops type
	 * @return json
	 */
	public static String getQueryJsonSchema(String opsType) {

		ObjectMapper mapper = JSONUtil.getDefaultObjectMapper();
		String json = null;
		try {
			SchemaFactoryWrapper wrapper = new SchemaFactoryWrapper();
			if (opsType.equals(DYNAMIC_UPDATE)) {
				mapper.acceptJsonFormatVisitor(UpdatePojo.class, wrapper);
			} else if (opsType.equals(DYNAMIC_DELETE)) {
				mapper.acceptJsonFormatVisitor(DeletePojo.class, wrapper);
			} else {
				mapper.acceptJsonFormatVisitor(QueryResponse.class, wrapper);
			}
			JsonSchema schema = wrapper.finalSchema();
			json = JSONUtil.prettyPrintJSON(schema);
		} catch (Exception e) {
			throw new ConnectorException("Failed to build Schema", e);
		}
		return json;
	}

	/**
	 * This method will iterate over varray elements and build the schema
	 * accordingly.
	 *
	 * @param array    the array
	 * @param sbSchema the sb schema
	 */
	public static void iterateOverVarrays(ArrayDescriptor array, StringBuilder sbSchema) {
		try {
			for (int i = 1; i <= array.getMaxLength(); i++) {
				sbSchema.append(BACKSLASH + ELEMENT + i + OPEN_PROPERTIES);
				if (array.getBaseName().equals(VARCHAR) || array.getBaseName().equalsIgnoreCase(DATE)) {
					sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
				} else if (array.getBaseName().equals(NUMBER)) {
					sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
				}
				sbSchema.append("},");
			}
		} catch (SQLException e) {
			throw new ConnectorException(e);
		}
	}

	/**
	 * This method will iterate over nested tables and get the column names and
	 * types of the inner table for building schema for all Dynamic Operations.
	 *
	 * @param array    the array
	 * @param sbSchema the sb schema
	 * @param con      the con
	 * @throws SQLException the SQL exception
	 */
	public static void iterateOverNestedTables(ArrayDescriptor array, StringBuilder sbSchema, Connection con)
			throws SQLException {

		StructDescriptor structLevel1 = StructDescriptor.createDescriptor(array.getBaseName(), con);
		for (int i = 1; i <= structLevel1.getMetaData().getColumnCount(); i++) {
			sbSchema.append(BACKSLASH + structLevel1.getMetaData().getColumnName(i) + OPEN_PROPERTIES);
			if (structLevel1.getMetaData().getColumnType(i) == 12 || structLevel1.getMetaData().getColumnType(i) == 91
					|| structLevel1.getMetaData().getColumnType(i) == 1) {
				sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
			} else if (structLevel1.getMetaData().getColumnType(i) == 2) {
				sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
			} else {
				sbSchema.append(TYPE_OBJECT);
				sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
				ArrayDescriptor arrayLevel2 = ArrayDescriptor
						.createDescriptor(structLevel1.getMetaData().getColumnTypeName(i), con);
				if (arrayLevel2.getBaseName().equals(NUMBER) || arrayLevel2.getBaseName().equals(VARCHAR)
						|| arrayLevel2.getBaseName().equalsIgnoreCase(DATE)) {
					sbSchema.append(BACKSLASH + structLevel1.getMetaData().getColumnName(i) + OPEN_PROPERTIES);
					sbSchema.append(TYPE_OBJECT);
					sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
					iterateOverVarrays(array, sbSchema);
					sbSchema.deleteCharAt(sbSchema.length() - 1);
					sbSchema.append('}');
				} else {
					StructDescriptor structLevel2 = StructDescriptor.createDescriptor(arrayLevel2.getBaseName(), con);
					for (int j = 1; j <= structLevel2.getMetaData().getColumnCount(); j++) {
						sbSchema.append(BACKSLASH + structLevel2.getMetaData().getColumnName(j) + OPEN_PROPERTIES);
						if (structLevel2.getMetaData().getColumnType(j) == 12
								|| structLevel2.getMetaData().getColumnType(j) == 91
								|| structLevel2.getMetaData().getColumnType(j) == 1) {
							sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
						} else if (structLevel2.getMetaData().getColumnType(j) == 2) {
							sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
						} else if (structLevel2.getMetaData().getColumnType(j) == 2003) {
							sbSchema.append(TYPE_OBJECT);
							sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
							ArrayDescriptor arrayLevel3 = ArrayDescriptor
									.createDescriptor(structLevel2.getMetaData().getColumnTypeName(j), con);
							if (arrayLevel3.getBaseName().equals(NUMBER) || arrayLevel3.getBaseName().equals(VARCHAR)
									|| arrayLevel3.getBaseName().equalsIgnoreCase(DATE)) {
								iterateOverVarrays(arrayLevel3, sbSchema);
								sbSchema.deleteCharAt(sbSchema.length() - 1);
								sbSchema.append('}');
							} else {
								throw new ConnectorException("nested level exhausted");
							}
						}
						sbSchema.append("},");
					}
					sbSchema.deleteCharAt(sbSchema.length() - 1);
					sbSchema.append('}');
				}
			}
			sbSchema.append("},");
		}
	}

	/**
	 * This method will iterate over nested tables and get the column names and
	 * types of the inner table for building schema for all Standard Operations.
	 *
	 * @param array    the array
	 * @param sbSchema the sb schema
	 * @param con      the con
	 * @throws SQLException the SQL exception
	 */
	public static void iterateOverStandardNestedTable(ArrayDescriptor array, StringBuilder sbSchema, Connection con)
			throws SQLException {

		StructDescriptor structLevel1 = StructDescriptor.createDescriptor(array.getBaseName(), con);
		for (int i = 1; i <= structLevel1.getMetaData().getColumnCount(); i++) {
			if (structLevel1.getMetaData().getColumnType(i) == 12 || structLevel1.getMetaData().getColumnType(i) == 91
					|| structLevel1.getMetaData().getColumnType(i) == 1) {
				sbSchema.append(BACKSLASH + structLevel1.getMetaData().getColumnName(i) + OPEN_PROPERTIES);
				sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
			} else if (structLevel1.getMetaData().getColumnType(i) == 2) {
				sbSchema.append(BACKSLASH + structLevel1.getMetaData().getColumnName(i) + OPEN_PROPERTIES);
				sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
			} else {
				ArrayDescriptor arrayLevel2 = ArrayDescriptor
						.createDescriptor(structLevel1.getMetaData().getColumnTypeName(i), con);
				StructDescriptor structLevel2 = StructDescriptor.createDescriptor(arrayLevel2.getBaseName(), con);
				for (int j = 1; j <= structLevel2.getMetaData().getColumnCount(); j++) {
					if (structLevel2.getMetaData().getColumnType(j) == 12
							|| structLevel2.getMetaData().getColumnType(j) == 91
							|| structLevel2.getMetaData().getColumnType(j) == 1) {
						sbSchema.append(BACKSLASH + structLevel2.getMetaData().getColumnName(j) + OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + STRING + BACKSLASH);
					} else if (structLevel2.getMetaData().getColumnType(j) == 2) {
						sbSchema.append(BACKSLASH + structLevel2.getMetaData().getColumnName(j) + OPEN_PROPERTIES);
						sbSchema.append(BACKSLASH + JSONUtil.SCHEMA_TYPE + DOUBLE_BACKSLASH + INTEGER + BACKSLASH);
					} else if (structLevel2.getMetaData().getColumnType(j) == 2003) {
						ArrayDescriptor arrayLevel3 = ArrayDescriptor
								.createDescriptor(structLevel2.getMetaData().getColumnTypeName(j), con);
						sbSchema.append(BACKSLASH + structLevel2.getMetaData().getColumnName(j) + OPEN_PROPERTIES);
						sbSchema.append(TYPE_OBJECT);
						sbSchema.append(" \"" + JSONUtil.SCHEMA_PROPERTIES + OPEN_PROPERTIES);
						iterateOverVarrays(arrayLevel3, sbSchema);
						sbSchema.deleteCharAt(sbSchema.length() - 1);
						sbSchema.append('}');
					}
					sbSchema.append("},");
				}
				sbSchema.deleteCharAt(sbSchema.length() - 1);
			}
			sbSchema.append("},");

		}
		sbSchema.deleteCharAt(sbSchema.length() - 1);
	}

	/**
	 * Gets the table names.
	 *
	 * @param objectTypeId the object type id
	 * @return the table names
	 */
	public static String[] getTableNames(String objectTypeId) {
		if (objectTypeId.contains(",")) {
			return objectTypeId.split("[,]", 0);
		} else {
			return new String[] { objectTypeId };
		}
	}
}
