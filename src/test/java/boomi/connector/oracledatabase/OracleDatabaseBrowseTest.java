package boomi.connector.oracledatabase;

import java.util.Arrays;

import org.junit.Test;

import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.oracledatabase.OracleDatabaseBrowser;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;

public class OracleDatabaseBrowseTest {
	

	@Test
	public void testgetObjectDefinitions_CREATE() {
		BrowserTest context = new BrowserTest();
		OracleDatabaseConnection conn = new OracleDatabaseConnection(context);
		conn.loadProperties();
		OracleDatabaseBrowser browser = new OracleDatabaseBrowser(conn);
		ObjectTypes objTypes= browser.getObjectTypes();
		ObjectDefinitions objDefinitions = browser.getObjectDefinitions("PERSON",   Arrays.asList(ObjectDefinitionRole.INPUT, ObjectDefinitionRole.OUTPUT));	
	}

}
