package boomi.connector.oracledatabase;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.DynamicUpdateOperation;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;
import com.boomi.connector.testutil.SimpleTrackedData;

public class DynamicUpdateTest {
	
	private BrowserTest context = new BrowserTest();
	private BrowseTestBatch contextBatch = new BrowseTestBatch();
	private OracleDatabaseConnection con = new OracleDatabaseConnection(context);
	private OracleDatabaseConnection conBatch = new OracleDatabaseConnection(contextBatch);
	private DynamicUpdateOperation ops = new DynamicUpdateOperation(con);
	private DynamicUpdateOperation opsBatch = new DynamicUpdateOperation(conBatch);
	private UpdateRequest request = mock(UpdateRequest.class);
	private OperationResponse response = mock(OperationResponse.class);
	private Logger logger = mock(Logger.class);
	

	public static final String input = "{\r\n" + "	\"PERSON_ID\": \"123\",\r\n" + "	\"NAME\": \"Swastik\",\r\n"
			+ "	\"PERSON_DOB\": \"12-MAR-2020\",\r\n" + "	\"CITY\": \"BANGALORE\",\r\n" + "	\"STATUS\": \"E\"\r\n"
			+ "}";
	
	@Before
	public void init() {
		when(response.getLogger()).thenReturn(logger);
	}
	


	@Test
	public void testexecuteCreateOperation() throws IOException {
		con.loadProperties();
		InputStream result = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		SimpleTrackedData trackedData = new SimpleTrackedData(1, result);
		Iterator<ObjectData> objDataItr = Mockito.mock(Iterator.class);
		when(request.iterator()).thenReturn(objDataItr);
		when(objDataItr.hasNext()).thenReturn(true, false);
		when(objDataItr.next()).thenReturn(trackedData);
		when(response.getLogger()).thenReturn(Mockito.mock(Logger.class));
		try(Connection connn = DriverManager.getConnection(con.getUrl(), con.getUsername(), con.getPassword())) {
			connn.setAutoCommit(false);
			ops.executeUpdateOperation(request, response, connn);
		} catch (SQLException e) {
			throw new ConnectorException(e.toString());
		}
		assertTrue(true);
		result.close();
	
	}
	
	@Test
	public void testexecuteUpdateOperation() throws IOException {
		con.loadProperties();
		InputStream result = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		SimpleTrackedData trackedData = new SimpleTrackedData(1, result);
		Iterator<ObjectData> objDataItr = Mockito.mock(Iterator.class);
		when(request.iterator()).thenReturn(objDataItr);
		when(objDataItr.hasNext()).thenReturn(true, false);
		when(objDataItr.next()).thenReturn(trackedData);
		when(response.getLogger()).thenReturn(Mockito.mock(Logger.class));
		try(Connection connn = DriverManager.getConnection(conBatch.getUrl(), conBatch.getUsername(), conBatch.getPassword())) {
			opsBatch.executeUpdateOperation(request, response, connn);
		} catch (SQLException e) {
			
		}
		assertTrue(true);
		result.close();
	
	
	}

}
