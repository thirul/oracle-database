package boomi.connector.oracledatabase;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;
import com.boomi.connector.oracledatabase.storedprocedureoperation.StoredProcedureOperation;
import com.boomi.connector.testutil.SimpleTrackedData;

public class StoredProcedureTest {
	
	private BrowseTestProcedure context = new BrowseTestProcedure();
	private OracleDatabaseConnection con = new OracleDatabaseConnection(context);
	private StoredProcedureOperation ops = new StoredProcedureOperation(con);

	private Logger logger = mock(Logger.class);
	
	UpdateRequest request = mock(UpdateRequest.class);
	OperationResponse response = mock(OperationResponse.class);
	
	public static final String input = "{\r\n" + 
			"	\"ST_DOB\": {\r\n" + 
			"		\"element 1\": \"\",\r\n" + 
			"		\"element 2\": \"\"\r\n" + 
			"	},\r\n" + 
			"	\"ST_ID\": {\r\n" + 
			"		\"element 1\": \"\",\r\n" + 
			"		\"element 2\": \"\"\r\n" + 
			"	},\r\n" + 
			"	\"ST_NAMES\": \"ramesh\"\r\n" + 
			"}";
	
	
	@Before
	public void init() {
		when(response.getLogger()).thenReturn(logger);
	}
	
	@Test
	public void testexecuteCreateOperation() throws IOException {
		con.loadProperties();
		InputStream result = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
		SimpleTrackedData trackedData = new SimpleTrackedData(1, result);
		Iterator<ObjectData> objDataItr = Mockito.mock(Iterator.class);
		when(request.iterator()).thenReturn(objDataItr);
		when(objDataItr.hasNext()).thenReturn(true, false);
		when(objDataItr.next()).thenReturn(trackedData);
		when(response.getLogger()).thenReturn(Mockito.mock(Logger.class));
		ops.executeSizeLimitedUpdate(request, response);
		assertTrue(true);
		result.close();
	}

}
