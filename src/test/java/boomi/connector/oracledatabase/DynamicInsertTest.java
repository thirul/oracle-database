package boomi.connector.oracledatabase;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Logger;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.oracledatabase.DynamicInsertOperation;
import com.boomi.connector.oracledatabase.OracleDatabaseConnection;
import com.boomi.connector.testutil.SimpleTrackedData;

public class DynamicInsertTest {

	private BrowserTest context = new BrowserTest();
	private OracleDatabaseConnection con = new OracleDatabaseConnection(context);
	private DynamicInsertOperation ops = new DynamicInsertOperation(con);
	private UpdateRequest request = mock(UpdateRequest.class);
	private OperationResponse response = mock(OperationResponse.class);
	private Logger logger = mock(Logger.class);
	static Random rand = new Random();
	static int id = rand.nextInt(1000);

	public static final String input = "{\r\n" + "	\"COUNTRIES\": {\r\n" + "		\"COUNTRY_ID\": \"IN\",\r\n"
			+ "		\"LOCATIONS\": {\r\n" + "			\"LOCATION\": \"y\",\r\n"
			+ "			\"LOCATION_ID\": 12,\r\n" + "			\"POSTAL_CODE\": \"567\",\r\n"
			+ "			\"STARTEND_DATE\": {\r\n" + "				\"element 1\": \"2020-11-11 00:00:00\",\r\n"
			+ "				\"element 2\": \"2020-11-11 00:00:00\"\r\n" + "			}\r\n" + "		}\r\n" + "	},\r\n"
			+ "	\"REGION_ID\": 11,\r\n" + "	\"REGION_NAME\": \"DAKSH\"\r\n" + "}";

	public static final String input2 = "{\r\n" + "	\"PERSON_ID\": \"+in+\",\r\n" + "	\"NAME\": \"Swastik\",\r\n"
			+ "	\"PERSON_DOB\": \"12-MAR-2020\",\r\n" + "	\"CITY\": \"BANGALORE\",\r\n" + "	\"STATUS\": \"E\"\r\n"
			+ "}";

	@Before
	public void init() {
		when(response.getLogger()).thenReturn(logger);
	}

	@Test
	public void testexecuteCreateOperation() throws IOException {
		con.loadProperties();
		InputStream result = new ByteArrayInputStream(input2.getBytes(StandardCharsets.UTF_8));
		SimpleTrackedData trackedData = new SimpleTrackedData(1, result);
		Iterator<ObjectData> objDataItr = Mockito.mock(Iterator.class);
		when(request.iterator()).thenReturn(objDataItr);
		when(objDataItr.hasNext()).thenReturn(true, false);
		when(objDataItr.next()).thenReturn(trackedData);
		when(response.getLogger()).thenReturn(Mockito.mock(Logger.class));
		ops.executeSizeLimitedUpdate(request, response);
		assertTrue(true);
		result.close();
	}

}
